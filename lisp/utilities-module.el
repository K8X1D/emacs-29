;;; .* --- .*"  -*- lexical-binding: t; -*-
;;; Commentary:
;;; Code:

;; Change opacity
(defun k8x1d/set-frame-opacity (opacity)
  "Interactively change the current frame's opacity (v29 pgtk version).
OPACITY is an integer between 0 to 100, inclusive."
  (interactive
   (list (read-number "Opacity (0-100): "
		      (or (frame-parameter nil 'alpha)
		    100))))
  (if (and (eq window-system 'pgtk) (>= emacs-major-version 29))
      (set-frame-parameter nil 'alpha-background opacity)
    (set-frame-parameter nil 'alpha opacity)))

;; Vms management
  (defun k8x1d/vm-view ()
    (interactive)
    (async-shell-command "virt-viewer --attach --connect qemu:///system kubuntu24.04")
    ;; (view-buffer "*Async Shell Command*")
    )
  (defun k8x1d/vm-start ()
    (interactive)
    (async-shell-command "virsh --connect qemu:///system start kubuntu24.04")
    (view-buffer "*Async Shell Command*")
    )
  (defun k8x1d/vm-stop ()
    (interactive)
    (async-shell-command "virsh --connect qemu:///system shutdown kubuntu24.04")
    (view-buffer "*Async Shell Command*")
    )
  (defun k8x1d/vm-list ()
    (interactive)
    (async-shell-command "virsh --connect qemu:///system list")
    (view-buffer "*Async Shell Command*")
    )

;; remove text property
(defun k8x1d/strip-text-properties (txt)
  (set-text-properties 0 (length txt) nil txt)
      txt)

;; Common terminal for WM
(defun k8x1d/switch-to-terminal (terminal-name)
  (require 'vterm)
  (if (get-buffer terminal-name)
      (switch-to-buffer terminal-name)
    (vterm terminal-name)
    )
  )

;; Create or swith to tab
(defun k8x1d/create-or-switch-tab-then (tab-name fun)
  (let* ((tab-status (not (eq (member tab-name 
				      (mapcar (lambda (tab)
						(alist-get 'name tab))
					      (funcall tab-bar-tabs-function)
					      )) nil))))
    (if tab-status
	(progn
	  (tab-bar-select-tab-by-name tab-name)
	  (funcall fun)
	  )
      (progn
	(tab-bar-switch-to-tab tab-name)
	(funcall fun)))
    ))


(provide 'utilities-module)
;;; utilities-module.el ends here
