# -*- mode: bash-ts; -*-
# emacs-client.sh --create-frame --eval "(progn (multi-vterm) (toggle-frame-tab-bar) (setq mode-line-format nil))"

PROJECT_NAME="emacs"
EMACS_DIR="$HOME/.config/$PROJECT_NAME"
"$EMACS_DIR/bin/emacs-client.sh" --create-frame

