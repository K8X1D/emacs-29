;;; .* --- .*"  -*- lexical-binding: t; -*-
;;; Commentary:
;;; Code:

;; RSS reader
(use-package newst-reader
  :general
  (k8x1d/leader-key
    "on" '(newsticker-show-news :which-key "RSS News")
   )
  :bind
  ("C-c o n" . newsticker-show-news)
  :hook
  (newsticker-mode . imenu-add-menubar-index)
  :config
  (setq newsticker-url-list '(
			      ("Thanos Apollo blog" "https://thanosapollo.org/posts/index.xml" nil nil nil)
			      ("Sacha Chua news" "https://sachachua.com/blog/category/emacs-news/feed"  nil nil nil)

			      ("Guix blog" "https://guix.gnu.org/feeds/blog.atom"  nil nil nil)
			      ("Distrowatch news" "https://distrowatch.com/news/dw.xml" nil nil nil)

			      ("Baeldung" "https://feeds.feedblitz.com/baeldung/linux&x=1")
			      ("Babolivier" "https://brendan.abolivier.bzh//index.xml")

			      ))
  )

(provide 'rss-module)
;;; rss-module.el ends here
