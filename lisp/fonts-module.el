;;; .* --- .*"  -*- lexical-binding: t; -*-
;;; Commentary:
;;; Code:

;; Fonts
(use-package emacs
  :custom-face
  (default ((t (:inherit nil :height 125 :family "Iosevka Nerd Font"))))
  (fixed-pitch ((t (:inherit nil :height 1.0 :family "Iosevka Nerd Font"))))
  (variable-pitch ((t (:inherit nil :height 1.0 :family "Iosevka Aile" :weight regular))))
  )

(provide 'fonts-module)
;;; fonts-module.el ends here
