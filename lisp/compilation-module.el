;;; package --- Summary  -*- lexical-binding: t; -*-
;;; Commentary:
;;; Code: 


(use-package compile
  :config
  (setq compilation-ask-about-save nil)
  (setq compilation-window-height 17)
  ;; Prettify compilation buffer
  ;; - inspiration: https://stackoverflow.com/questions/3072648/cucumbers-ansi-colors-messing-up-emacs-compilation-buffer
  (require 'ansi-color)
  (defun colorize-compilation-buffer ()
    (let ((inhibit-read-only t))
      (ansi-color-apply-on-region (point-min) (point-max))))
  ;; (add-hook 'compilation-filter-hook 'colorize-compilation-buffer)
  ;; (add-hook 'TeX-output-mode-hook 'colorize-compilation-buffer)
  :hook
  (compilation-filter . colorize-compilation-buffer)
  (TeX-output-mode . colorize-compilation-buffer)
  )

(provide 'compilation-module)
;;; compilation-module.el ends here
