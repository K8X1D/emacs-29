;;; .* --- .*"  -*- lexical-binding: t; -*-
;;; Commentary:
;;; Code:

(use-package dired
  :general
  (k8x1d/leader-key
    "od"  '(dired :which-key "File explorer")
    )
  :hook
  (dired-mode . dired-hide-details-mode) ;; don't show all information at once
  :config
  (setq dired-hide-details-hide-symlink-targets nil) ;; show symlink destination
  ;; (setq dired-listing-switches "-al") ;; in test
  (setq dired-auto-revert-buffer t)
  (setq dired-guess-shell-alist-user '(("\\.docx\\'" "libreoffice")
				       ("\\.xlsx\\'" "libreoffice")
				       ("\\.odt\\'" "libreoffice")
				       ("\\.ods\\'" "libreoffice")
				       ("\\.csv\\'" "libreoffice")
				       ("\\.tsv\\'" "libreoffice")
				       ("\\.rtf\\'" "libreoffice")
				       ("\\.mp4\\'" "mpv")
				       ("\\.mkv\\'" "mpv")
				       ("\\.webm\\'" "mpv")
				       ("\\.pdf\\'" "zathura")
				       ("\\.html\\'" "firefox")
				       ("\\.jpeg\\'" "gimp")
				       ("\\.png\\'" "gimp")
				       ))
  (setq dired-dwim-target t) ;; Allow dired tries to guess a default target directory, see https://emacs.stackexchange.com/questions/5603/how-to-quickly-copy-move-file-in-emacs-dired

  ;; from https://stackoverflow.com/questions/13901955/how-to-avoid-pop-up-of-async-shell-command-buffer-in-emacs
  ;; Don't open stderr buffer when opening file with comande
  (add-to-list 'display-buffer-alist
	       (cons "\\*Async Shell Command\\*.*" (cons #'display-buffer-no-window nil)))
  )


;; Don't show "." and ".." paths
(use-package dired-x
  :hook
  (dired-mode . dired-omit-mode)
  :config
  (setq dired-omit-verbose nil)
  )


;; Dired with a better ui
(use-package dirvish
  :disabled
  :general
  (k8x1d/leader-key
    "od"  '(dirvish :which-key "File explorer")
    )
  :init
  (dirvish-override-dired-mode)
  :config
  (setq dirvish-attributes
	'(vc-state subtree-state nerd-icons collapse git-msg file-time file-size))
  (setq dirvish-subtree-state-style 'nerd)
  ;; (setq dirvish-attributes
	;; '(nerd-icons collapse file-time file-size))
	;; '(nerd-icons file-size))
  ;; (setq dirvish-subtree-state-style 'nerd)
  )


(use-package dirvish
  :disabled
  :init
  (require 'dirvish)
  :general
  (k8x1d/leader-key
    "od"  '(dirvish :which-key "File explorer")
    )
  :hook
  (after-init . dirvish-override-dired-mode)
  :custom
  (dirvish-quick-access-entries ; It's a custom option, `setq' won't work
   '(("h" "~/"                          "Home")
     ("d" "~/Downloads/"                "Downloads")
     ))
  :config
  ;; (dirvish-peek-mode) ; Preview files in minibuffer
  ;; (dirvish-side-follow-mode) ; similar to `treemacs-follow-mode'
  (setq dirvish-mode-line-format
        '(:left (sort symlink) :right (omit yank index)))
  (setq dirvish-attributes
	'(vc-state subtree-state nerd-icons collapse git-msg file-time file-size))
  (setq dirvish-subtree-state-style 'nerd)
  ;; (setq delete-by-moving-to-trash t)
  (setq dired-listing-switches
        "-l --almost-all --human-readable --group-directories-first --no-group")
  :bind ; Bind `dirvish|dirvish-side|dirvish-dwim' as you see fit
  (("C-c f" . dirvish-fd)
   :map dirvish-mode-map ; Dirvish inherits `dired-mode-map'
   ;; ("q"   . dirvish-quit)
   ("a"   . dirvish-quick-access)
   ("f"   . dirvish-file-info-menu)
   ("y"   . dirvish-yank-menu)
   ("N"   . dirvish-narrow)
   ("^"   . dirvish-history-last)
   ("h"   . dirvish-history-jump) ; remapped `describe-mode'
   ("s"   . dirvish-quicksort)    ; remapped `dired-sort-toggle-or-edit'
   ("v"   . dirvish-vc-menu)      ; remapped `dired-view-file'
   ("TAB" . dirvish-subtree-toggle)
   ("M-f" . dirvish-history-go-forward)
   ("M-b" . dirvish-history-go-backward)
   ("M-l" . dirvish-ls-switches-menu)
   ("M-m" . dirvish-mark-menu)
   ("M-t" . dirvish-layout-toggle)
   ("M-s" . dirvish-setup-menu)
   ("M-e" . dirvish-emerge-menu)
   ("M-j" . dirvish-fd-jump)))


;; Group files
(use-package dirvish-emerge
  :disabled
  :config
  (add-hook 'dirvish-setup-hook  #'dirvish-emerge-mode)
  (setq dirvish-emerge-groups
	'(("Recent files" (predicate . recent-files-2h))
	  ("Documents" (extensions "pdf" "tex" "bib" "epub"))
	  ("Scripts" (extensions "sh" "R" "jl" "py" "el" "scm"))
	  ("Video" (extensions "mp4" "mkv" "webm"))
	  ("Pictures" (extensions "jpg" "png" "svg" "gif" "jpeg"))
	  ("Audio" (extensions "mp3" "flac" "wav" "ape" "aac"))
	  ("Archives" (extensions "gz" "rar" "zip"))))
  )


;; Pdf viewing
(use-package pdf-tools
  :after dirvish
  )

(provide 'file-explorer-module)
;;; file-explorer-module.el ends here
