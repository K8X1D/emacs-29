;;; .* --- .*"  -*- lexical-binding: t; -*-
;;; Commentary:
;;; Code:


;; File-explorer in sidebar
(use-package dired-sidebar
  ;; :after nerd-icons-dired
  ;; :preface
  ;; (defun sidebar-toggle ()
  ;;   "Toggle both `dired-sidebar' and `ibuffer-sidebar'."
  ;;   (interactive)
  ;;   (dired-sidebar-toggle-sidebar)
  ;;   (ibuffer-sidebar-toggle-sidebar))
  :bind
  (:map project-prefix-map
	;; ("e" . sidebar-toggle))
	("e" . dired-sidebar-toggle-sidebar))
  :commands (dired-sidebar-toggle-sidebar)
  :hook
  (dired-sidebar-mode . (lambda ()
			  (unless (file-remote-p default-directory)
			  (auto-revert-mode)))) ;; Do not autorevert when tramp connection
  :config
  (push 'toggle-window-split dired-sidebar-toggle-hidden-commands)
  (push 'rotate-windows dired-sidebar-toggle-hidden-commands)

  (setq dired-sidebar-subtree-line-prefix "  ")
  (setq dired-sidebar-theme 'nerd-icons)
  ;; (setq dired-sidebar-theme 'nerd)
  (setq dired-sidebar-use-term-integration t)
  (setq dired-sidebar-use-custom-font t)
  )

;; Buffer management in sidebar
(use-package ibuffer-sidebar
  :disabled
  :commands (ibuffer-sidebar-toggle-sidebar)
  :config
  (setq ibuffer-sidebar-use-custom-font t)
  (setq ibuffer-sidebar-face `(:family "Helvetica" :height 140))
  )

;; Project configuration
(use-package project
  :general
  (k8x1d/leader-key
    "p" '(:keymap project-prefix-map
		  :which-key "Project")
   )
  ;; :bind*
  ;; (:map k8x1d/leader-map
  ;; 	;; ("p" ("Projects" . project-prefix-map))
  ;; 	("p" . project-prefix-map)
  ;; 	)
  :config
  (setq project-compilation-buffer-name-function (lambda (mode) (concat "*" (downcase mode) "-" (project-name (project-current)) "*"))) ;; rename compilation buffer
  (setq project-buffers-viewer #'project-list-buffers-ibuffer) ;; ibuffer in replacement of default
  )


(provide 'project-module)
;;; file-explorer-module.el ends here
