;;; .* --- .*"  -*- lexical-binding: t; -*-
;;; Commentary:
;;; Code:

;; HTML web Browser
(use-package eww
  :preface
  (defun k8x1d/open-browser ()
    (interactive)
    (k8x1d/create-or-switch-tab-then "Browser" #'(lambda () (eww (completing-read "Enter URL or keywords: " nil))))
    )
  :general
  (k8x1d/leader-key
    "ob"  '(k8x1d/open-browser :which-key "Browser")
    )
  :bind
  ("C-c o b" . eww)
  (:map k8x1d/leader-map
	("ob" ("Browser" . eww))
	)
  (:map eww-mode-map
	([remap eww-list-buffers] . tab-bar-switch-to-next-tab)
	([remap ignore] . evil-insert)
	)
)

;; Nyxt interaction
(use-package nyxt
  :general
  (k8x1d/leader-key
    "oN"  '(:ignore t :which-key "Nyxt")
    "oNi"  '(nyxt-init :which-key "Init")
    "oNs"  '(nyxt-init :which-key "Search")
    )
)

(provide 'browser-module)
;;; browser-module.el ends here
