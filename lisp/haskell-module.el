;;; .* --- .*"  -*- lexical-binding: t; -*-
;;; Commentary:
;;; Code:


;; Syntax highlight
(use-package haskell-mode)

;; LSP
(use-package eglot
  :hook
  (haskell-mode . eglot-ensure)
  )


;; Checker
(use-package flymake
  :hook
  (haskell-mode . eglot-ensure)
  )

;; Snippets
(use-package haskell-snippets
  :hook
  (haskell-mode . yas-minor-mode)
  )

(provide 'haskell-module)
;;; haskell-module.el ends here
