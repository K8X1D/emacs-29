;;; .* --- .*"  -*- lexical-binding: t; -*-
;;; Commentary:
;;; Code:


;; dispay character as fancy symbol
(use-package prog-mode 
  :hook
  (prog-mode . prettify-symbols-mode)
  )

(provide 'prog-module)
;;; prog-module.el ends here
