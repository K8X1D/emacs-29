;;; .* --- .*"  -*- lexical-binding: t; -*-
;;; Commentary:
;;; Code:

(use-package pdf-tools
  ;; :mode "\\.pdf\\'"
  :init
  (pdf-loader-install)
  :hook 
  (pdf-view-mode . pdf-view-fit-page-to-window)
  :config
  (setq pdf-view-display-size 'fit-page)
  )



;; Native viewing, In test... Too slow for pdf... Kept for docx/odt
(use-package doc-view
  :config
  (setq doc-view-resolution 196)
  )

(provide 'pdf-module)
;;; pdf-module.el ends here
