;;; .* --- .*"  -*- lexical-binding: t; -*-
;;; Commentary:
;;; Code:

(use-package gptel
  :preface
  (defun k8x1d/start-ollama ()
    (interactive)
    ;; (async-shell-command "podman start ollama")
    (async-shell-command "ollama serve")
    )
  (defun k8x1d/stop-ollama ()
    (interactive)
    (async-shell-command "podman stop ollama")
    )
  :general
  (k8x1d/leader-key
   "c"  '(:ignore t :which-key "Chatbot")
   "ca"  '(gptel :which-key "Chatbot")
   "cs"  '(k8x1d/start-ollama :which-key "Start")
   )
  :bind
  (:map k8x1d/leader-map
	("oc" ("Chatbot" . gptel))
	)
  :config
  ;;  (setq gptel-log-level 'debug)
  (setq gptel-use-curl t)
  (setq gptel-model 'mistral:latest)
  (setq-default gptel-backend (gptel-make-ollama "Ollama"
				:host "localhost:11434"
				:stream t
				:models '(mistral:latest)))
  )


(provide 'chatbot-module)
;;; chatbot-module.el ends here
