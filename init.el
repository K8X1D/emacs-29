;;; .* --- .*"  -*- lexical-binding: t; -*-
;;; Commentary:
;;; Code:

;; Benchmark
;; (use-package benchmark-init
;;   :init
;;   (benchmark-init/activate)
;;   :hook
;;   (after-init . benchmark-init/deactivate)
;;   )

;; Set scratch message via file
;; from https://emacs.stackexchange.com/questions/38708/how-to-replace-the-default-contents-of-the-scratch-buffer-with-the-contents-of-a
(let ((filename (concat user-emacs-directory "reminder.txt")))
  (when (file-exists-p filename)
    (let ((scratch-buf (get-buffer "*scratch*")))
      (when scratch-buf
	(with-current-buffer scratch-buf
	  (erase-buffer)
	  (insert-file-contents filename))))))

(require 'variables-module)
(if k8x1d/timed-startup (message (format "Variables : %s" (emacs-init-time))))

;; Base
;; (load "keybindings-module")
(if k8x1d/timed-startup (message (format "Base (init) : %s" (emacs-init-time))))
(require 'keybindings-module)
(require 'modal-editing-module)
(require 'editing-module)
(require 'buffers-module)
(require 'bookmarks-module)
(require 'file-explorer-module)
(require 'navigation-module)
(require 'package-module)
(require 'indents-module)
(require 'utilities-module)
(require 'windows-module)
(require 'todos-module)
(require 'documentation-module)
(require 'remote-module)
(if k8x1d/timed-startup (message (format "Base : %s" (emacs-init-time))))

;; Gui
(if k8x1d/timed-startup (message (format "Gui (init) : %s" (emacs-init-time))))
(require 'themes-module)
(require 'fonts-module)
(require 'modeline-module)
(require 'tab-module)
(require 'completion-module)
(require 'icons-module)
(require 'treesitter-module)
(require 'highlight-module)
(require 'scrolling-module)
(require 'line-numbers-module)
(if k8x1d/timed-startup (message (format "Gui : %s" (emacs-init-time))))

;; Project
(if k8x1d/timed-startup (message (format "Project (init) : %s" (emacs-init-time))))
(require 'project-module)
(require 'compilation-module)
(require 'snippets-module)
(require 'lsp-module)
(require 'checker-module)
(if k8x1d/timed-startup (message (format "Project : %s" (emacs-init-time))))

;; ;; Programming
(if k8x1d/timed-startup (message (format "Programming (init) : %s" (emacs-init-time))))
(require 'prog-module)
(require 'elisp-module)
(require 'notebook-module)
(require 'r-module)
(require 'julia-module)
(require 'haskell-module)
(require 'python-module)
(require 'yaml-module)
(require 'php-module)
(require 'docker-module)
(require 'version-control-module)
(if k8x1d/timed-startup (message (format "Programming : %s" (emacs-init-time))))

;; Writing
(if k8x1d/timed-startup (message (format "Writing (init) : %s" (emacs-init-time))))
(require 'text-module)
(require 'corrector-module)
(require 'markdown-module)
(require 'latex-module)
(require 'csv-module)
(require 'org-module)
(if k8x1d/timed-startup (message (format "Writing : %s" (emacs-init-time))))

;; ;; Utilities
(if k8x1d/timed-startup (message (format "Utilities (init) : %s" (emacs-init-time))))
(require 'guix-module)
(require 'terminal-module)
(require 'irc-module)
(require 'rss-module)
(require 'browser-module)
(require 'process-manager-module)
(require 'torrents-module)
(require 'calendar-module)
(require 'pdf-module)
(require 'password-module)
(require 'bibliography-module)
(require 'container-module)
(require 'desktop-module)
(require 'social-module)
(require 'bluetooth-module)
(require 'chatbot-module)
(require 'blog-module)
(require 'notes-module)
(require 'emails-module)

;; FIXME: one or more cause tui to not show entries in vertico
(require 'multimedia-module)
(if k8x1d/timed-startup (message (format "Utilities : %s" (emacs-init-time))))

(add-hook 'after-init-hook '(lambda () 
			      (message (format "Emacs started in %s" (emacs-init-time)))
			      ))
;;; init.el ends here

