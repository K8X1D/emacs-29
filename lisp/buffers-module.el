;;; package --- Summary  -*- lexical-binding: t; -*-
;;; Commentary:
;;; Code:

;; Buffer managements
(use-package ibuffer
  :bind
  ([remap list-buffers] . ibuffer)
  :hook
  (ibuffer-mode . ibuffer-auto-mode)
  :config
  ;; adapted from https://www.emacswiki.org/emacs/IbufferMode#h5o-6
  (setq ibuffer-saved-filter-groups
	'(("Home"
	   ;; Projects
	   ("K8X1D-guix"     (or (filename . "k8x1d-guix")
				 (name . "k8x1d-guix")))
	   ("Emacs"     (or (filename . "emacs")
			    (name . "emacs")))
	   ("DH-HPS"     (or (filename . "blackwell_dh_hps")
			     (name . "blackwell_dh_hps")))
	   ("Monitoring"     (or (filename . "Monitoring")
				 (name . "Monitoring")))
	   ("Socius"     (or (filename . "SOCIUS")
			     (name . "SOCIUS")))
	   ("OFDIG"     (or (filename . "OFDIG")
			    (name . "OFDIG")))
	   ;; Tramp connections
	   ("Podman" (or  (name . "tramp/podman")
			  (filename . "podman")))
	   ("SSH" (or  (name . "tramp/ssh")
		       (filename . "ssh")))

	   ;; Rest
	   ("LSP" (name . "EGLOT .+"))
	   ("Terminal" (or (mode . vterm-mode)
			   (name . "*vterminal")))
	   ("Mail" (or (mode . notmuch-show-mode)
		       (name . "notmuch")))
	   ("Journal" (name . "[0-9]\\{4\\}\\-[0-9]\\{2\\}\\-[0-9]\\{2\\}.org"))
	   ("Notes" (filename . "/org/"))
	   ("Stars"   (starred-name))
	   ("Modified" (predicate buffer-modified-p (current-buffer))) ;; must be last
	   )))

  ;; Tell ibuffer to load the group automatically
  (add-hook 'ibuffer-mode-hook
	    (lambda ()
	      (ibuffer-switch-to-saved-filter-groups "Home")
	      ))
  )
;; see https://cestlaz.github.io/posts/using-emacs-34-ibuffer-emmet/

;; Keep buffers updated with corresponding files
(use-package autorevert
  :hook
  (after-init . global-auto-revert-mode)
  :config
  (setq auto-revert-use-notify nil)
  (setq auto-revert-remote-files nil)
  (setq auto-revert-verbose nil)
  (setq global-auto-revert-non-file-buffers t) ;; Revert other buffers (e.g, Dired)
  )

;; Keybindings
(use-package emacs
  :general
  (k8x1d/leader-key
   "b"  '(:ignore t :which-key "Buffers")
   "bs"  '(switch-to-buffer :which-key "Select")
   "bb"  '(list-buffers :which-key "List")
   "bk"  '(kill-buffer :which-key "Kill")
   "f"  '(:ignore t :which-key "File")
   "fs"  '(save-buffer :which-key "Save")
   "ff"  '(find-file :which-key "Find")
   )
  :bind*
  (:map k8x1d/leader-map
	;; ("b" ("Buffers" . keymap))
	("bs" ("Select" . switch-to-buffer))
	("bb" ("List" . list-buffers))
	("bk" ("Kill" . kill-buffer))
	("fs" ("Save" . save-buffer))
	("ff" ("Find" . find-file))
	)
  :config
  (keymap-set k8x1d/leader-map "b" '("Buffers" . (keymap)))
  (keymap-set k8x1d/leader-map "f" '("File" . (keymap)))
  )

;; Auto-save buffer
(use-package files
  :init
  (setq view-read-only t)
  (setq auto-save-visited-interval 30)
  :hook
  (after-init . auto-save-visited-mode) ;; autosave to original file
  :config
  ;; Enable auto-save to safeguard against crashes or data loss. The
  ;; `recover-file' or `recover-session' functions can be used to restore
  ;; auto-saved data.
  (setq auto-save-default t)

  ;; Do not auto-disable auto-save after deleting large chunks of
  ;; text. The purpose of auto-save is to provide a failsafe, and
  ;; disabling it contradicts this objective.
  (setq auto-save-include-big-deletions t)

  (setq auto-save-list-file-prefix
	(expand-file-name "autosave/" user-emacs-directory))
  (setq tramp-auto-save-directory
	(expand-file-name "tramp-autosave/" user-emacs-directory))

  ;; Auto save options
  (setq kill-buffer-delete-auto-save-files t)
  )

(provide 'buffers-module)
;;; buffers-module.el ends here
