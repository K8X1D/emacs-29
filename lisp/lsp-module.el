;;; .* --- .*"  -*- lexical-binding: t; -*-
;;; Commentary:
;;; Code:

;; Native lsp support
;; FIXME : prevent flymake indication, find problematic parameter
(use-package eglot
  :commands (eglot
	     eglot-rename
	     eglot-ensure
	     eglot-rename
	     eglot-format
	     eglot-format-buffer)
  :config
  (setq eglot-report-progress nil)  ; Prevent minibuffer spam
  ;; (setq eglot-stay-out-of '(flymake)) ;; cause probleme with eglot-ltex, indications are not shown.

  ;; Optimizations, see https://www.reddit.com/r/emacs/comments/1b25904/is_there_anything_i_can_do_to_make_eglots/
  (fset #'jsonrpc--log-event #'ignore)
  ;; (eglot-events-buffer-size 0)
  (setq eglot-sync-connect nil)
  (setq eglot-connect-timeout nil)
  (setq eglot-autoshutdown t)
  (setq eglot-send-changes-idle-time 3)
  (setq flymake-no-changes-timeout 5)
  ;;(eldoc-echo-area-use-multiline-p nil)
  (setq eglot-ignored-server-capabilities '(:documentHighlightProvider))
  (setq eglot-menu-string "lsp")
  :bind
  ("C-c l a" . eglot-code-actions)
  ("C-c l s" . eglot-shutdown)
  ("C-c l S" . eglot-shutdown-all)
  ("C-c l r" . eglot-reconnect)
  ("C-c l f" . eglot-format)
  ("C-c l F" . eglot-format-buffer)
  )

;; Better performance for eglot
(use-package eglot-booster
  :hook
  (after-init . eglot-booster-mode)
  :config
  (setq eglot-booster-no-remote-boost t)
  )

;; LSP-MODE do too much at the same time for my need.
;; LSP via LSP-mode
(use-package lsp-mode
  :disabled
  :config
  (setq lsp-headerline-breadcrumb-enable nil)
  (setq lsp-modeline-workspace-status-enable nil)
  (setq lsp-modeline-diagnostics-enable nil)
  (setq lsp-modeline-code-actions-enable nil)
  (setq lsp-modeline-code-actions-enable nil)
  )

;; UI interface for lsp-mode
(use-package lsp-ui
  :disabled
  )

(use-package lsp-ltex
  :disabled
  :init
  (setq lsp-ltex-language "fr")
  (setq lsp-ltex-version "15.2.0"))



(provide 'lsp-module)
;;; lsp-module.el ends here
