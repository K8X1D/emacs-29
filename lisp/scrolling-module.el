;;; .* --- .*"  -*- lexical-binding: t; -*-
;;; Commentary:
;;; Code:

;; Smoother scrolling
;; (use-package pixel-scroll
;;   :disabled
;;   :hook
;;   (after-init . pixel-scroll-precision-mode)
;;   :config
;;   (setq pixel-scroll-precision-use-momentum t)
;;   (setq pixel-scroll-precision-momentum-seconds 0.5)

;;   ;; scroll one line at a time (less "jumpy" than defaults)
;;   (setq mouse-wheel-scroll-amount '(1 ((shift) . 1))) ;; one line at a time
;;   (setq mouse-wheel-progressive-speed nil) ;; don't accelerate scrolling
;;   (setq mouse-wheel-follow-mouse 't) ;; scroll window under mouse
;;   (setq scroll-step 1) ;; keyboard scroll one line at a time

;;   (setq scroll-conservatively 10000)
;;   )


;; ;; Smoother scrolling
(use-package ultra-scroll
  :hook
  (after-init . ultra-scroll-mode)
  :config
  (setq scroll-conservatively 101 ; important!
	scroll-margin 0)
  )

(provide 'scrolling-module)
;;; scrolling-module.el ends here

