
;;; .* --- .*"  -*- lexical-binding: t; -*-
;;; Commentary:
;;; Code:

;; Syntax highlight
(use-package markdown-mode
  :mode "\\.md\\'"
  :hook
  (markdown-mode . variable-pitch-mode)
  :config
  (setq markdown-enable-highlighting-syntax t)
  (setq markdown-hide-urls nil)
  (setq markdown-enable-math t)
  (setq markdown-enable-wiki-links t)
  (setq markdown-enable-html t)
  (setq-default markdown-hide-markup nil)
  (setq markdown-list-item-bullets '("‣"))
  ;; (setq markdown-command "multimarkdown")
  (setq markdown-command "markdown")

  ;; (custom-set-faces
  ;;  `(markdown-header-face-1 ((t (:inherit outline-1 :height 1.0))))
  ;;  `(markdown-header-face-2 ((t (:inherit outline-2 :height 1.0))))
  ;;  `(markdown-header-face-3 ((t (:inherit outline-3 :height 1.0))))
  ;;  `(markdown-header-face-4 ((t (:inherit outline-4 :height 1.0))))
  ;;  `(markdown-header-face-5 ((t (:inherit outline-5 :height 1.0))))
  ;;  `(markdown-header-face-6 ((t (:inherit outline-6 :height 1.0))))
  ;;  `(markdown-header-face-7 ((t (:inherit outline-7 :height 1.0))))
  ;;  `(markdown-header-face-8 ((t (:inherit outline-8 :height 1.0))))
  ;;  )
  :custom-face
  ;; Adjust font by section 
   (markdown-table-face ((t (:inherit 'fixed-pitch))))
   (markdown-code-face ((t (:inherit 'fixed-pitch))))
  ;; Use different color by heading level 
   (markdown-header-face-1 ((t (:inherit outline-1 :height 1.0 :family "Iosevka Nerd Font"))))
   (markdown-header-face-2 ((t (:inherit outline-2 :height 1.0 :family "Iosevka Nerd Font"))))
   (markdown-header-face-3 ((t (:inherit outline-3 :height 1.0 :family "Iosevka Nerd Font"))))
   (markdown-header-face-4 ((t (:inherit outline-4 :height 1.0 :family "Iosevka Nerd Font"))))
   (markdown-header-face-5 ((t (:inherit outline-5 :height 1.0 :family "Iosevka Nerd Font"))))
   (markdown-header-face-6 ((t (:inherit outline-6 :height 1.0 :family "Iosevka Nerd Font"))))
   (markdown-header-face-7 ((t (:inherit outline-7 :height 1.0 :family "Iosevka Nerd Font"))))
   (markdown-header-face-8 ((t (:inherit outline-8 :height 1.0 :family "Iosevka Nerd Font"))))
  )

;; Treesitter support
(use-package markdown-ts-mode
:disabled
  :mode ("\\.md\\'" . markdown-ts-mode)
  :config
  (add-to-list 'treesit-language-source-alist '(markdown "https://github.com/tree-sitter-grammars/tree-sitter-markdown" "split_parser" "tree-sitter-markdown/src"))
  (add-to-list 'treesit-language-source-alist '(markdown-inline "https://github.com/tree-sitter-grammars/tree-sitter-markdown" "split_parser" "tree-sitter-markdown-inline/src")))

;; Automatic table of content
(use-package toc-org
  :hook
  (markdown-mode . toc-org-mode))


(provide 'markdown-module)
;;; markdown-module.el ends here
