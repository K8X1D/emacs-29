;;; package --- Summary  -*- lexical-binding: t; -*-
;;; Commentary:
;;; Code:

;; Highlight current line
(use-package hl-line
  :hook
  (prog-mode . hl-line-mode)
)

;; better hightlight for hl-line
(use-package lin
  :disabled
  :hook
  (after-init . lin-global-mode)
  :config
  (setq lin-face 'lin-blue) ; check doc string for alternative styles
  ;; You can use this to live update the face:
  ;;
  ;; (customize-set-variable 'lin-face 'lin-green)
  (setq lin-mode-hooks
	'(bongo-mode-hook
	  dired-mode-hook
	  elfeed-search-mode-hook
	  git-rebase-mode-hook
	  grep-mode-hook
	  ibuffer-mode-hook
	  ilist-mode-hook
	  ledger-report-mode-hook
	  log-view-mode-hook
	  magit-log-mode-hook
	  mu4e-headers-mode-hook
	  notmuch-search-mode-hook
	  notmuch-tree-mode-hook
	  occur-mode-hook
	  org-agenda-mode-hook
	  pdf-outline-buffer-mode-hook
	  proced-mode-hook
	  tabulated-list-mode-hook))
  )

;; Highlight colors name
(use-package rainbow-mode
  :hook
  (prog-mode . rainbow-mode))

;; Show indentations
(use-package indent-bars
  :hook (prog-mode . indent-bars-mode))

(use-package highlight-indent-guides
  :disabled
  :hook
  (prog-mode . highlight-indent-guides-mode)
  :config
  (setq highlight-indent-guides-method 'bitmap)
  ;; (setq highlight-indent-guides-method 'character)
  (setq highlight-indent-guides-responsive 'stack)
  )


;; diff highlight
(use-package diff-hl
  :disabled
  :hook
  (after-init . global-diff-hl-mode)
  )

(provide 'highlight-module)
;;; highlight-module.el ends here
