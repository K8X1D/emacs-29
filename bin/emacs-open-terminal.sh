# -*- mode: bash-ts; -*-

PROJECT_NAME="emacs"
EMACS_DIR="$HOME/.config/$PROJECT_NAME"
# "$EMACS_DIR/bin/emacs-client.sh" --create-frame --eval "(progn (multi-vterm) (setq mode-line-format nil))"
# "$EMACS_DIR/bin/emacs-client.sh" --create-frame --eval "(progn (multi-vterm) (toggle-frame-tab-bar) (setq mode-line-format nil))"
"$EMACS_DIR/bin/emacs-client.sh" --create-frame --eval "(progn (k8x1d/switch-to-terminal \"*wm-terminal*\") (toggle-frame-tab-bar) (setq mode-line-format nil) (k8x1d/swith-theme-to (car custom-enabled-themes) nil))"


