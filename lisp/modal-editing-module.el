;;; .* --- .*"  -*- lexical-binding: t; -*-
;;; Commentary:
;;; Code:

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Vim modal editing emulator
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(use-package evil
  :init
  (setq evil-want-keybinding nil)     ;; Disable default keybinding to set custom ones.
  (setq evil-undo-system 'undo-redo) ;; native undo-redo
  (setq evil-want-Y-yank-to-eol t)
  (setq evil-want-integration t)      ;; Integrate `evil' with other Emacs features (optional as it's true by default).
  (setq evil-want-C-u-scroll t)       ;; Makes C-u scroll
  (setq evil-want-C-u-delete t)       ;; Makes C-u delete on insert mode
  (setq evil-echo-state nil) ;; do not show state in echo buffer
  :general
  (k8x1d/leader-key
   "w" '(:keymap evil-window-map
		  :package evil
		  :which-key "Window")
   )
  :hook
  (after-init . evil-mode)
  (git-commit-mode . evil-insert-state)
  (org-capture-mode . evil-insert-state)
  (vc-git-log-edit-mode . evil-insert-state)
  :config
  ;; (keymap-set evil-normal-state-map "SPC" k8x1d/leader-map) ;; replaced by general.el; easier for overridding
  ;; (keymap-set evil-normal-state-map "SPC m" my-localleader-map)  
  ;; (add-to-list 'emulation-mode-map-alists 'evil-normal-state-map) ;; ensure that leader-map ha higher priority

  (evil-select-search-module 'evil-search-module 'evil-search)
  (setq evil-mode-line-format nil) ;; managed manually
  )

;; Evil support extension
(use-package evil-collection
  :hook
  (evil-mode . evil-collection-init)
  )

;; Org integration
(use-package evil-org
  :hook
  (org-mode . evil-org-mode)
  (org-agenda-mode . evil-org-mode)
  :config
  (require 'evil-org-agenda)
  (evil-org-set-key-theme '(navigation insert textobjects additional calendar))
  (evil-org-agenda-set-keys)
  (require 'org-agenda)
  (keymap-set org-agenda-mode-map "<remap> <org-agenda-show-tags>" #'tab-bar-switch-to-next-tab)
  )

;; Keybindings correction
(use-package evil
  :after vertico
  :bind
  (:map vertico-map
	("?" . minibuffer-completion-help)
	("C-j" . vertico-next)
	("C-k" . vertico-previous)
	("M-h" . vertico-directory-up))
  )

;; Leader key
(use-package evil
  :disabled
  :config
  (evil-define-key 'normal 'org-mode-map (kbd "<localleader>t") 'org-todo)
  (evil-define-key 'normal 'org-mode-map (kbd "<localleader>ci") 'org-clock-in)
  (evil-define-key 'normal 'org-mode-map (kbd "<localleader>co") 'org-clock-out)

  )

(provide 'modal-editing-module)
;;; modal-editing-module.el ends here
