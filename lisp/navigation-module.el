;;; .* --- .*"  -*- lexical-binding: t; -*-
;;; Commentary:
;;; Code:



;; Menus for accessing locations in documents
(use-package imenu
  :config
  (setq imenu-auto-rescan t)
  (setq imenu-use-popup-menu nil)
  )






(provide 'navigation-module)
;;; navigation-module.el ends here
