;;; .* --- .*"  -*- lexical-binding: t; -*-
;;; Commentary:
;;; Code:

;; Snippets configuration
(use-package yasnippet)

;; Snippets collection
(use-package yasnippet-snippets)

(provide 'snippets-module)
;;; snippets-module.el ends here
