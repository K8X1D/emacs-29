;;; package --- Summary  -*- lexical-binding: t; -*-
;;; Commentary:
;;; Code:

;; Syntax highlight
(use-package php-ts-mode
  :mode "\\.php\\'"
  :config
    (add-to-list 'treesit-language-source-alist
	'(php "https://github.com/tree-sitter/tree-sitter-php"))
  )


(provide 'php-module)
;;; php-module.el ends here
