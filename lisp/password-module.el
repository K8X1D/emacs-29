;;; .* --- .*"  -*- lexical-binding: t; -*-
;;; Commentary:
;;; Code:

;; Interact with pass
(use-package pass
  :general
  (k8x1d/leader-key
   "op"  '(pass :which-key "Password Manager") 
   )
  :bind
  (:map k8x1d/leader-map
	("op" ("Pass" . pass))
	)
  )

(provide 'password-module)
;;; password-module.el ends here
