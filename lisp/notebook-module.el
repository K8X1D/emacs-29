;;; .* --- .*"  -*- lexical-binding: t; -*-
;;; Commentary:
;;; Code:

;; Notebook
(use-package org
  :config
  (setq org-edit-src-content-indentation 0) ;; not indentation
  (setq org-confirm-babel-evaluate nil) ;; don't ask at each block for evaluation

  ;; Languages supported
  (add-to-list 'org-babel-load-languages '(julia . t))
  (add-to-list 'org-babel-load-languages '(R . t))
  (add-to-list 'org-babel-load-languages '(python . t))
  (add-to-list 'org-babel-load-languages '(shell . t))
  (add-to-list 'org-babel-load-languages '(emacs-lisp . t))
  (add-to-list 'org-babel-load-languages '(latex . t))
  (add-to-list 'org-babel-load-languages '(sql . t))
  (add-to-list 'org-babel-load-languages '(scheme . t))
  (org-babel-do-load-languages 'org-babel-load-languages org-babel-load-languages)
  )

;; ;; Async execution for org-babel
;; FIXME: repair ess-startup directory blocking process
(use-package ob-async
  :disabled
  :hook
  (org-mode . (lambda () (require 'ob-async)))
  (ob-async-pre-execute-src-block . (lambda ()
				      ;; (require 'ess)
				      ;; (require 'ess-custom)
				      ;; (setq ess-startup-directory "/tmp")
				      (require 'ess-julia)
				      (setq ess-ask-for-ess-directory nil)
				      (setq-default inferior-julia-program "/home/k8x1d/.guix-home/profile/bin/julia")
				      (setq inferior-julia-program "/home/k8x1d/.guix-home/profile/bin/julia")
				      (setq-local inferior-julia-program "/home/k8x1d/.guix-home/profile/bin/julia")))
  :config
  (add-to-list 'ob-async-no-async-languages-alist "ipython")
  (add-to-list 'ob-async-no-async-languages-alist "jupyter-julia")
  (add-to-list 'ob-async-no-async-languages-alist "jupyter-python")
  (add-to-list 'ob-async-no-async-languages-alist "jupyter-R")
  (require 'ess)
  ;; (setq ess-startup-directory "/tmp")
  (setq ess-ask-for-ess-directory nil)
  )


;; Notebook support through jupyter
(use-package jupyter
  :disabled
  :bind
  ("C-c r j" . jupyter-run-repl)
  )
;; Adjust babel support for jupyter
(use-package org
  :disabled
  :config
  ;; (add-to-list 'org-babel-load-languages '(jupyter . t))
  ;; (org-babel-do-load-languages 'org-babel-load-languages org-babel-load-languages)
  (require 'jupyter-R)
  (require 'jupyter-python)
  (require 'jupyter-julia)
  (org-babel-do-load-languages
   'org-babel-load-languages
   '((emacs-lisp . t)
     (julia . t)
     (python . t)
     (jupyter . t)))
  :init
  ;; diplay ansi color instead of character
;; https://github.com/emacs-jupyter/jupyter/issues/465
  (defun display-ansi-colors ()
    "Fixes kernel output in emacs-jupyter"
    (ansi-color-apply-on-region (point-min) (point-max)))
  :hook
  (org-babel-after-execute-hook . display-ansi-colors)
  (org-mode . (lambda ()
		;; Replace org-babel names
		(org-babel-jupyter-override-src-block "python")
		(org-babel-jupyter-override-src-block "julia")
		(org-babel-jupyter-override-src-block "R")
		))
  )

(provide 'notebook-module)
;;; notebook-module.el ends here
