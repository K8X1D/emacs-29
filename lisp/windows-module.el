
;;; .* --- .*"  -*- lexical-binding: t; -*-
;;; Commentary:
;;; Code:

;; Move to window with arrow keys
(use-package windmove
  :config
  ;; (windmove-default-keybindings)
  (setq windmove-default-keybindings 'control)
  )

;; Include window to undo/redo
(use-package winner
  :hook
  (after-init . winner-mode)
  )

;; Better `other window`
;; Not much faster than windmove
(use-package ace-window
  :disabled
  :bind
  ([remap other-window] . ace-window)
  )

;; Delimit window
(use-package frame
  :hook
  (after-init . window-divider-mode)
  :config
  ;; (setq window-divider-default-places t)
  (setq window-divider-default-right-width 1)
  (setq window-divider-default-bottom-width 1)
  (setq window-divider-default-places 'right-only)
  ;; (setq window-divider-default-bottom-width 2)
  ;; Add frame border
  (modify-all-frames-parameters
   '(;;(right-divider-width . 0)
     (internal-border-width . 20)))
  )



(provide 'windows-module)
;;; windows-module.el ends here
 
