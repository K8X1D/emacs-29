;;; .* --- .*"  -*- lexical-binding: t; -*-
;;; Commentary:
;;; Code:

;; Syntax highlight
(use-package julia-mode
  :disabled
  :mode "\\.jl$"
  )

;; ;; Treesitter support
;; ;; FIXME: cause problem with eldoc... Documentation not available
;; ;; correct issue https://github.com/JuliaEditorSupport/julia-ts-mode/issues/25
(use-package julia-ts-mode
  :mode "\\.jl$"
  :config
  ;; (add-to-list 'treesit-language-source-alist
  ;; 	       '(julia "https://github.com/tree-sitter/tree-sitter-julia"))
  (add-to-list 'major-mode-remap-alist
	       '((julia-mode . julia-ts-mode)))
  )

;; REPL
(use-package julia-vterm
  :general
  (k8x1d/leader-key
    "orj"  '(julia-vterm-repl :which-key "Julia")
    )
  :bind
  ("C-c r j" . julia-vterm-repl)
  :hook
  (julia-mode . julia-vterm-mode)
  (julia-ts-mode . julia-vterm-mode)
  :config
  (setq julia-vterm-repl-program "julia --quiet --color=yes --banner=no")
  )

;; Diagnostic
(use-package flymake
  :hook
  (julia-mode . flymake-mode)
  (julia-ts-mode . flymake-mode)
  )

;; LSP
(use-package eglot
  :init
  (setq eglot-connect-timeout 180)
  :hook
  (julia-mode . eglot-ensure) ;; lsp
  (julia-ts-mode . eglot-ensure) ;; lsp
  :config
  ;; set lsp program
  (add-to-list 'eglot-server-programs
	       '((julia-mode julia-ts-mode) . (
					       "julia"
					       "--startup-file=no"
					       "--history-file=no"
					       ;; "--project=/home/k8x1d/.julia/environments/emacs"
					       ;; "--project=/home/k8x1d/.julia/environments/nvim-lspconfig"
					       "--project=/homd/k8x1d/.julia/packages/LanguageServer/Fwm1f/src"
					       "-e"
					       "using LanguageServer; runserver()"
					       ))
	       )
  )

;; Snippets
(use-package yasnippet
  :hook
  (julia-mode . yas-minor-mode)
  (julia-ts-mode . yas-minor-mode)
  )

;; Notebook support
(use-package org
  :config
  (add-to-list 'org-babel-load-languages '(julia . t))
  (add-to-list 'org-src-lang-modes '("julia" . julia-ts))
  (org-babel-do-load-languages 'org-babel-load-languages org-babel-load-languages)
  )

(use-package ob-julia-vterm)
(use-package org
  :config
  (require 'ob-julia-vterm)
  (add-to-list 'org-babel-load-languages '(julia-vterm . t))
  (org-babel-do-load-languages 'org-babel-load-languages org-babel-load-languages)
  (defalias 'org-babel-execute:julia 'org-babel-execute:julia-vterm)
  (defalias 'org-babel-variable-assignments:julia 'org-babel-variable-assignments:julia-vterm)
  )


(provide 'julia-module)
;;; julia-module.el ends here
