;;; .* --- .*"  -*- lexical-binding: t; -*-
;;; Commentary:
;;; Code:


;; Configure theme
(use-package doom-themes
  :init 
  (setq k8x1d/dark-theme 'doom-gruvbox)
  (setq k8x1d/light-theme 'doom-gruvbox-light)
  (setq k8x1d/initial-theme k8x1d/dark-theme)
  :hook
  (after-init . (lambda () (k8x1d/swith-theme-to k8x1d/initial-theme nil 80)))
  :config
  (setq doom-themes-enable-bold t) ; if nil, bold is universally disabled
  (setq doom-themes-enable-italic t) ; if nil, bold is universally disabled
  :preface
  (defun k8x1d/set-org-colors ()
    (require 'org)
    ;; TODOS faces
    (setq org-todo-keyword-faces
	  `(("TODO" . (:inherit fixed-pitch :foreground ,(doom-color 'red) :weight bold))
	    ("WAIT" . (:inherit fixed-pitch :foreground ,(doom-color 'yellow) :weight bold))
	    ("NEXT" . (:inherit fixed-pitch :foreground ,(doom-color 'teal) :weight bold))
	    ("DONE" . (:inherit fixed-pitch :foreground ,(doom-color 'grey) :weight bold))
	    ("CNCL" . (:inherit fixed-pitch :foreground ,(doom-color 'grey) :weight bold)))
	  )
    ;; (org-mode-restart)
    )
  (defun k8x1d/swith-theme-to (theme full opacity)
    "Switch theme to THEME."
    (if full
	(disable-theme (car custom-enabled-themes))
      )
    (load-theme theme t)
    (k8x1d/set-custom-colors)
    (k8x1d/set-org-colors)
    ;; (k8x1d/set-frame-opacity opacity)
    (set-frame-parameter nil 'alpha-background opacity)
    )
  (defun k8x1d/themes-toggle (dark-theme light-theme)
    "Toggle between two themes."
    (let* ((current-theme (car custom-enabled-themes)))
      (if (eq current-theme dark-theme) 
	  (k8x1d/swith-theme-to light-theme t 90))
      (if (eq current-theme light-theme) 
	  (k8x1d/swith-theme-to dark-theme t 80))
      ))
  (defun k8x1d/themes-toggle-dark-light ()
    "Toggle between light/dark theme."
    (interactive)
    (k8x1d/themes-toggle k8x1d/dark-theme k8x1d/light-theme)
    (k8x1d/set-custom-colors)
    )
  (defun k8x1d/set-custom-colors ()
    ;; Adjust faces
    (custom-set-faces
     ;; Icomplete
     `(icomplete-selected-match ((t (:foreground ,(doom-color 'orange) :background ,(doom-color 'bg)))))
     ;; `(icomplete-selected-match ((t (:foreground ,(doom-color 'orange) :background ,(doom-color 'bg)))))

     ;; Modeline
     `(mode-line-active ((t (:overline ,(doom-color 'fg) :underline (:color ,(doom-color 'fg) :position -5 )))))
     `(mode-line-inactive ((t (:background ,(doom-color 'bg-alt) :foreground ,(doom-color 'bg-alt)))))

     ;; Tab-bar
     ;; `(tab-bar ((t (:foreground ,(doom-color 'fg) :height 1.0))))
     `(tab-bar ((t (:foreground ,(doom-color 'fg) :height 1.25 :underline (:color ,(doom-color 'fg) :position -5)))))
     `(tab-bar-tab ((t (:foreground ,(doom-color 'fg)))))
     `(tab-bar-tab-inactive ((t (:foreground ,(doom-color 'grey)))))
     ;; `(tab-bar-tab-inactive ((t (:foreground ,(doom-color 'bg-alt)))))

     ;; `(tab-bar-echo-area-tab ((t (:foreground ,(doom-color 'fg) :underline (:color ,(doom-color 'fg) :position -5 )))))
     ;; `(tab-bar-echo-area-tab ((t (:foreground ,(doom-color 'fg) :box (:color ,(doom-color 'fg) :style flat-button)))))

     ;; Windows
     `(window-divider ((t (:foreground ,(doom-color 'fg)))))

     ;; Checker
     `(flymake-note ((t (:underline (:color ,(doom-color 'green) :style line)))))
     `(flymake-warning ((t (:underline (:color ,(doom-color 'yellow) :style line)))))
     `(flymake-error ((t (:underline (:color ,(doom-color 'red) :style line)))))

     ;; LSP
     `(eglot-mode-line ((t (:foreground ,(doom-color 'orange)))))

     ;; Org
     ;; `(org-tag ((t (:foreground ,(doom-color 'bg) :background ,(doom-color 'blue) :box nil))))
     `(org-tag ((t (:foreground ,(doom-color 'green) :box t))))
     `(org-block-begin-line ((t (:foreground ,(doom-color 'green) :background ,(doom-color 'bg-alt)))))
     `(org-block-end-line ((t (:foreground ,(doom-color 'green)  :background ,(doom-color 'bg-alt)))))

     ))
  :bind
  ("<f5>" . k8x1d/themes-toggle-dark-light)
  )

(provide 'themes-module)
;;; themes-module.el ends here
