(use-modules (guix packages)
	     (guix git-download)
	     (guix download)
	     (guix build-system emacs)
	     (gnu packages emacs-xyz)
	     (guix build-system gnu)
	     (guix build-system cmake)
	     (gnu packages emacs)
	     (gnu packages statistics)
	     (gnu packages version-control)
	     (gnu packages perl)
	     (gnu packages texinfo)
	     (gnu packages cmake)
	     (gnu packages autotools)
	     (gnu packages terminals)
	     ((guix licenses) #:prefix license:)
	     )

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Packages definitions
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(define-public emacs-tab-bookmark
  (package
   (name "emacs-tab-bookmark")
   (version "20250101")
   (source
    (origin
     (method git-fetch)
     (uri (git-reference
	   (url "https://github.com/minad/tab-bookmark.git")
	   (commit
	    "aaaeb028a7271104a5b6d2c0b4d14eb11925bdf0")))
     (sha256
      (base32
       "1zm5169pv4g83zv6whkij0ypjz252sxg8r908rli4fpdhhh1rdw7"))))
   (build-system emacs-build-system)
   (propagated-inputs (list emacs-lsp-mode))
   (home-page
    "https://github.com/minad/tab-bookmark")
   (synopsis "Persist Emacs Tabs as Bookmarks")
   (description
    "Persist Emacs Tabs as Bookmarks.")
   (license #f)))

(define-public emacs-lsp-ltex
  (package
   (name "emacs-lsp-ltex")
   (version "20250131.1652")
   (source
    (origin
     (method git-fetch)
     (uri (git-reference
	   (url "https://github.com/emacs-languagetool/lsp-ltex.git")
	   (commit
	    "fc394cf8779e86e2d14c7ed209dafe225be90a6f")))
     (sha256
      (base32
       "1vhd9pm4445nf068g600dlnpnriq0a3ks69r5xj3wdic5l34yfvi"))))
   (build-system emacs-build-system)
   (propagated-inputs (list emacs-lsp-mode))
   (home-page
    "https://github.com/emacs-languagetool/lsp-ltex")
   (synopsis "LSP Clients for LTEX")
   (description
    "Documentation at https://melpa.org/#/lsp-ltex")
   (license #f)))


(define-public emacs-lsp-julia
  (package
   (name "emacs-lsp-julia")
   (version "20230915.654")
   (source
    (origin
     (method git-fetch)
     (uri (git-reference
	   (url "https://github.com/gdkrmr/lsp-julia.git")
	   (commit
	    "c869b2f6c05a97e5495ed3cc6710a33b4faf41a2")))
     (sha256
      (base32
       "0mbqv324b0km1z4af4hcgbc0493bizrbxr9v97asxvgm8j8bh24p"))))
   (build-system emacs-build-system)
   (propagated-inputs
    (list emacs-lsp-mode emacs-julia-mode))
   (arguments
    `(#:include (cons* "^languageserver/.*" %default-include)
      ))
   (home-page "https://github.com/gdkrmr/lsp-julia")
   (synopsis "Julia support for lsp-mode")
   (description
    "Documentation at https://melpa.org/#/lsp-julia")
   (license #f)))


(define-public emacs-highlight-indent-guides
  (let ((version "20241229") ; from package metadata
        (revision "0")
        (commit "3205abe2721053416e354a1ff53947dd122a6941"))
    (package
      (name "emacs-highlight-indent-guides")
      (version (git-version version revision commit))
      (source
       (origin
         (method git-fetch)
         (uri (git-reference
               (url "https://github.com/DarthFennec/highlight-indent-guides")
               (commit commit)))
         (file-name (git-file-name name version))
         (sha256
          (base32 "1w8qqgvviwd439llfpsmcihjx4dz5454r03957p2qi66f1a7g6vw"))))
      (build-system emacs-build-system)
      (home-page
       "https://github.com/DarthFennec/highlight-indent-guides")
      (synopsis "Minor mode to highlight indentation")
      (description
       "This package provides a minor mode to highlight indentation levels via
font-lock.  Indent widths are dynamically discovered, which means this
correctly highlights in any mode, regardless of indent width, even in
languages with non-uniform indentation such as Haskell.  This mode works
properly around hard tabs and mixed indentation and behaves well in large
buffers.")
      (license license:expat))))


(define-public emacs-org2blog
  (package
    (name "emacs-org2blog")
    (version "20250204")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/org2blog/org2blog.git")
               (commit
                 "2310990794c80de8c01bba8c8a7ad5b012c0705d")))
        (sha256
          (base32
            "0b9h9wjazqrd0wjjf4fpwxgg9gpfcm8pab83ymmm1m9x7cdq4ps8"))))
    (build-system emacs-build-system)
    (propagated-inputs
     (list emacs-htmlize emacs-hydra emacs-xml-rpc emacs-writegood-mode))
    (home-page
      "https://github.com/org2blog/org2blog")
    (synopsis "Blog from Org mode to WordPress.")
    (description
      "Blog from Org mode to WordPress.")
    (license #f)))

(define-public emacs-magit-file-icons
  (package
    (name "emacs-magit-file-icons")
    (version "20250204")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/gekoke/magit-file-icons.git")
               (commit
                 "85e4bc0184eb34cd2a799c3749889c6f74f80bea")))
        (sha256
          (base32
            "1yjm2xy00vsq2d14w4xcpriiaraav07i389vbx6g8i79bajgln25"))))
    (build-system emacs-build-system)
    (propagated-inputs
     (list emacs-nerd-icons emacs-el-patch))
    (home-page
      "https://github.com/gekoke/magit-file-icons.git")
    (synopsis "File icons for Magit")
    (description
      "File icons for Magit")
    (license #f)))

(define-public emacs-magit
  (package
    (name "emacs-magit")
    (version "20250217")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/magit/magit")
             (commit "12e1f3838cd6879fa79f5548eba5e80ad19d94f9")))
       (file-name (git-file-name name version))
       (sha256
        (base32 "06kkw7w5sim7yny0agy2in3yb1lfqwga987grxdyv0jv9z9kk6cm"))))
    (build-system emacs-build-system)
    (arguments
     (list
      #:tests? #t
      #:test-command #~(list "make" "test")
      #:exclude #~(cons* "magit-libgit.el"
                         "magit-libgit-pkg.el"
                         %default-exclude)
      #:phases
      #~(modify-phases %standard-phases
          (add-after 'unpack 'build-info-manual
            (lambda _
              (invoke "make" "info")
              ;; Copy info files to the lisp directory, which acts as
              ;; the root of the project for the emacs-build-system.
              (for-each (lambda (f)
                          (install-file f "lisp"))
                        (find-files "docs" "\\.info$"))))
          (add-after 'build-info-manual 'set-magit-version
            (lambda _
              (make-file-writable "lisp/magit.el")
              (emacs-substitute-variables "lisp/magit.el"
                ("magit-version" #$version))))
          (add-after 'set-magit-version 'patch-exec-paths
            (lambda* (#:key inputs #:allow-other-keys)
              (for-each make-file-writable
                        (list "lisp/magit-git.el" "lisp/magit-sequence.el"))
              (emacs-substitute-variables "lisp/magit-git.el"
                ("magit-git-executable"
                 (search-input-file inputs "/bin/git")))
              (emacs-substitute-variables "lisp/magit-sequence.el"
                ("magit-perl-executable"
                 (search-input-file inputs "/bin/perl")))))
          (add-before 'check 'configure-git
            (lambda _
              ;; Otherwise some tests fail with error "unable to auto-detect
              ;; email address".
              (setenv "HOME" (getcwd))
              (invoke "git" "config" "--global" "user.name" "toto")
              (invoke "git" "config" "--global" "user.email"
                      "toto@toto.com")))
          (replace 'expand-load-path
            (lambda args
              (with-directory-excursion "lisp"
                (apply (assoc-ref %standard-phases 'expand-load-path) args))))
          (replace 'make-autoloads
            (lambda args
              (with-directory-excursion "lisp"
                (apply (assoc-ref %standard-phases 'make-autoloads) args))))
          (replace 'install
            (lambda args
              (with-directory-excursion "lisp"
                (apply (assoc-ref %standard-phases 'install) args))))
          (replace 'build
            (lambda args
              (with-directory-excursion "lisp"
                (apply (assoc-ref %standard-phases 'build) args)))))))
    (native-inputs
     (list texinfo))
    (inputs
     (list git perl))
    (propagated-inputs
     ;; Note: the 'git-commit' and 'magit-section' dependencies are part of
     ;; magit itself.
     (list emacs-compat emacs-llama emacs-dash emacs-transient emacs-with-editor))
    (home-page "https://magit.vc/")
    (synopsis "Emacs interface for the Git version control system")
    (description
     "With Magit, you can inspect and modify your Git repositories
with Emacs.  You can review and commit the changes you have made to
the tracked files, for example, and you can browse the history of past
changes.  There is support for cherry picking, reverting, merging,
rebasing, and other common Git operations.")
    (license license:gpl3+)))


;; (define-public emacs-magit
;;   (package
;;     (name "emacs-magit")
;;     (version "20250217")
;;     (source
;;       (origin
;;         (method git-fetch)
;;         (uri (git-reference
;;                (url "https://github.com/magit/magit.git")
;;                (commit
;;                  "12e1f3838cd6879fa79f5548eba5e80ad19d94f9")))
;;         (sha256
;;           (base32
;;             "06kkw7w5sim7yny0agy2in3yb1lfqwga987grxdyv0jv9z9kk6cm"))))
;;     (build-system melpa-build-system)
;;     (propagated-inputs
;;       (list emacs-compat
;;             emacs-llama
;;             emacs-magit-section
;;             emacs-with-editor))
;;     (arguments
;;       '(#:files
;;         ("lisp/magit*.el"
;;          "lisp/git-*.el"
;;          "docs/magit.texi"
;;          "docs/AUTHORS.md"
;;          "LICENSE"
;;          ".dir-locals.el"
;;          (:exclude "lisp/magit-section.el"))))
;;     (home-page "https://github.com/magit/magit")
;;     (synopsis "A Git porcelain inside Emacs")
;;     (description
;;       "Documentation at https://melpa.org/#/magit")
;;     (license #f)))


(define-public emacs-dirvish
  (package
    (name "emacs-dirvish")
    (version "20250202")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/alexluigit/dirvish.git")
               (commit
                 "c0b435c4a546e879d2029fd77b94d710bb8dc8d1")))
        (sha256
          (base32
            "0knkr1s9xylhzgddg4mh8gmfp4ibzl4c1z614s301hcj2614mkab"))))
    (build-system emacs-build-system)
    (arguments
     (list
      #:phases
      #~(modify-phases %standard-phases
          ;; Move the extensions source files to the top level, which
          ;; is included in the EMACSLOADPATH.
          (add-after 'unpack 'move-source-files
            (lambda _
              (let ((el-files (find-files "./extensions" ".*\\.el$")))
                (for-each (lambda (f)
                            (rename-file f (basename f)))
                          el-files)))))))
    (home-page
      "https://github.com/alexluigit/dirvish")
    (synopsis
      "A modern file manager based on dired mode")
    (description
      "Documentation at https://melpa.org/#/dirvish")
    (license #f)))

(define-public emacs-flymake-languagetool
  (package
    (name "emacs-flymake-languagetool")
    (version "20250101.852")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/emacs-languagetool/flymake-languagetool.git")
               (commit
                 "c9b6749c5b17a8e58931cf2ce3ab5cbb855ae75d")))
        (sha256
          (base32
            "12sjiwxngv069c08vz2x8n49kcpwf7qc679lf7gh44dmyj0fs8fw"))))
    (build-system emacs-build-system)
    (propagated-inputs (list emacs-compat))
    (home-page
      "https://github.com/emacs-languagetool/flymake-languagetool")
    (synopsis "Flymake support for LanguageTool")
    (description
      "Documentation at https://melpa.org/#/flymake-languagetool")
    (license #f)))

(define-public emacs-eglot-ltex
  (package
    (name "emacs-eglot-ltex")
    (version "20250131")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/emacs-languagetool/eglot-ltex.git")
               (commit
                 "4bc35d72e18f66c15bb628829aa27e0d00a96785")))
        (sha256
          (base32
            "0hcx777lpcxrvnp11fllijpikj40vm708lzmw8w8slrrmqarpwi7"))))
    (build-system emacs-build-system)
    (propagated-inputs
     (list emacs-f))
    (home-page
      "https://github.com/emacs-languagetool/eglot-ltex")
    (synopsis "Eglot Clients for LTEX")
    (description
      "Eglot Clients for LTEX")
    (license #f)))

(define-public emacs-tab-bar-echo-area
  (package
    (name "emacs-tab-bar-echo-area")
    (version "20240809.1442")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/fritzgrabo/tab-bar-echo-area.git")
               (commit
                 "9ccff3b93385796bec1cd435674807c3907436dd")))
        (sha256
          (base32
            "0kn9ccsmqf10v3vnvdp0dh9qiqryrgkwly879z7v5mkzd6jp4ngr"))))
    (build-system emacs-build-system)
    (home-page
      "https://github.com/fritzgrabo/tab-bar-echo-area")
    (synopsis
      "Display tab names of the tab bar in the echo area")
    (description
      "Documentation at https://melpa.org/#/tab-bar-echo-area")
    (license #f)))


(define-public emacs-echo-bar
  (package
    (name "emacs-echo-bar")
    (version "20240601")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/benzanol/echo-bar.el.git")
               (commit
                 "80f5a8bbd8ac848d4a69796c9568b4a55958e974")))
        (sha256
          (base32
            "1mpq8ha42lffzzwy0ib8vbb2dp9fgqnh112wfa1a6b3vh21wnxm8"))))
    (build-system emacs-build-system)
    (home-page
      "https://github.com/benzanol/echo-bar.el")
    (synopsis
      "Display some custom text at the end of the echo area")
    (description
      "Documentation at https://melpa.org/#/echo-bar")
    (license #f)))


(define-public emacs-vterm
  (package
   (name "emacs-vterm")
   ;; (version "20241212")
   (version "20250113")
   ;; (version "20241218")
   (source (origin
	    (method git-fetch)
	    (uri (git-reference
		  (url "https://github.com/akermu/emacs-libvterm")
		  ;; (commit "09d019243d22434cc28708628126ec6e09aa5e7b")))
		  (commit "056ad74653704bc353d8ec8ab52ac75267b7d373")))
		  ;; (commit "c1a30176d4ba8c33e25c9dc841b141f3bf0ddadd")))
	    (file-name (git-file-name name version))
	    (sha256
	     (base32
	      ;; "17hkqb9dwnk28ad1synh00k55zps4bcsm7dynbzyzig0rr7xhwwl"))))
	      "0mh1xx3ggrr3kampm1b9cxydbn6csihipaa2bpjv08py98wi0434"))))
   ;; "195ldpnlqb6lkqqns04l8gvpdqz8xz8hwwgp4x7fb3iqpjamkh0l"))))
   (build-system emacs-build-system)
   ;; (build-system cmake-build-system)
   ;; (build-system gnu-build-system)
   ;; (arguments
   ;;  (list
   ;;   #:tests? #f
   ;;   #:phases
   ;;   #~(modify-phases %standard-phases
   ;; 		    (delete 'install)
   ;; 		    (delete 'build)
   ;; 		    (delete 'check)
   ;; 		    (delete 'configure)
   ;; 		    )
   ;;   ))
   (arguments
    `(#:modules ((guix build emacs-build-system)
		 ((guix build cmake-build-system) #:prefix cmake:)
		 (guix build emacs-utils)
		 (guix build utils))
      #:imported-modules (,@%emacs-build-system-modules
			  (guix build cmake-build-system))
      ;; Include the `etc' folder for shell-side configuration files
      #:include (cons* "^etc/.*" %default-include)
      #:phases
      (modify-phases %standard-phases
		     (add-after 'unpack 'substitute-vterm-module-path
				(lambda* (#:key outputs #:allow-other-keys)
				  (chmod "vterm.el" #o644)
				  (emacs-substitute-sexps "vterm.el"
							  ("(require 'vterm-module nil t)"
							   `(module-load
							     ,(string-append (assoc-ref outputs "out")
									     "/lib/vterm-module.so"))))))
		     (add-after 'build 'configure
				;; Run cmake.
				(lambda* (#:key outputs #:allow-other-keys)
				  ((assoc-ref cmake:%standard-phases 'configure)
				   #:outputs outputs
				   #:out-of-source? #f
				   #:configure-flags '("-DUSE_SYSTEM_LIBVTERM=ON"))))
		     (add-after 'configure 'make
				;; Run make.
				(lambda* (#:key (make-flags '()) outputs #:allow-other-keys)
				  ;; Compile the shared object file.
				  (apply invoke "make" "all" make-flags)
				  ;; Move the file into /lib.
				  (install-file
				   "vterm-module.so"
				   (string-append (assoc-ref outputs "out") "/lib"))))
		     ;; In test
		     (delete 'patch-el-files)
		     )
      #:tests? #f
      ))
   (native-inputs
    (list cmake-minimal libtool libvterm))
   (home-page "https://github.com/akermu/emacs-libvterm")
   (synopsis "Emacs libvterm integration")
   (description "This package implements a bridge to @code{libvterm} to
display a terminal in an Emacs buffer.")
   (license license:gpl3+)))

(define-public emacs-use-package
  ;; XXX: Upstream did not tag latest release.  Using commit matching exact
  ;; version bump.
  (let ((commit "a6e856418d2ebd053b34e0ab2fda328abeba731c"))
    (package
      (name "emacs-use-package")
      (version "2.4.4")
      (source (origin
		(method git-fetch)
                (uri (git-reference
                      (url "https://github.com/jwiegley/use-package")
                      (commit commit)))
                (file-name (git-file-name name version))
                (sha256
                 (base32
                  "0g1smk27ry391gk8bb8q3i42s0p520zwhxfnxvzv5cjj93mcpd8f"))))
      (build-system emacs-build-system)
      (arguments
       (list
        #:tests? #t
        #:test-command #~(list "emacs" "--batch"
                               "-l" "use-package-tests.el"
                               "-f" "ert-run-tests-batch-and-exit")
        ;; #:phases
        ;; #~(modify-phases %standard-phases
        ;;     (add-before 'install 'install-manual
        ;;       (lambda _
        ;;         (let ((info-dir (string-append #$output "/share/info")))
        ;;           (install-file "use-package.info" info-dir))))
        ;;     (add-before 'install-manual 'build-manual
        ;;       (lambda _
        ;;         (invoke "makeinfo" "use-package.texi"))))
	))
      (native-inputs
       (list texinfo))
      (propagated-inputs
       (list emacs-diminish))
      (home-page "https://github.com/jwiegley/use-package")
      (synopsis "Declaration for simplifying your .emacs")
      (description "The use-package macro allows you to isolate package
configuration in your @file{.emacs} file in a way that is both
performance-oriented and tidy.")
      (license license:gpl2+))))

(define-public emacs-consult-todo
  (package
    (name "emacs-consult-todo")
    (version "20250123.1915")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/liuyinz/consult-todo.git")
               (commit
                 "f031b38ff1806ad466dcbf0467b786eb3601b1c6")))
        (sha256
          (base32
            "138ns70wfmqj6nfi493yq07a3p6bdk96ci29y3wz4y06cxnvfyln"))))
    (build-system emacs-build-system)
    (propagated-inputs
      (list emacs-consult emacs-hl-todo))
    (home-page
      "https://github.com/eki3z/consult-todo")
    (synopsis "Search hl-todo keywords in consult")
    (description
      "Documentation at https://melpa.org/#/consult-todo")
    (license #f)))

(define-public emacs-ibuffer-sidebar
  (package
   (name "emacs-ibuffer-sidebar")
   (version "20210508.836")
   (source
    (origin
     (method git-fetch)
     (uri (git-reference
           (url "https://github.com/jojojames/ibuffer-sidebar.git")
           (commit
            "fb685e1e43db979e25713081d8ae4073453bbd5e")))
     (sha256
      (base32
       "04x87gngmvyj4nfq1dm3h9jr6b0kvikxsg1533kdkz9k72khs3n3"))))
   (build-system emacs-build-system)
   (home-page
    "https://github.com/jojojames/ibuffer-sidebar")
   (synopsis "Sidebar for `ibuffer'")
   (description
    "Documentation at https://melpa.org/#/ibuffer-sidebar")
   (license #f)))

(define-public emacs-nerd-icons-completion
  (package
   (name "emacs-nerd-icons-completion")
   (version "20240731.1213")
   (source
    (origin
     (method git-fetch)
     (uri (git-reference
           (url "https://github.com/rainstormstudio/nerd-icons-completion.git")
           (commit
            "426a1d7c29a04ae8e6ae9b55b0559f11a1e8b420")))
     (sha256
      (base32
       "03kkyxc9v38v1fc69xqc70gwvsq4pr8bgsk8f6is9z2w7p4y08sm"))))
   (build-system emacs-build-system)
   (propagated-inputs
    (list emacs-nerd-icons emacs-compat))
   (home-page
    "https://github.com/rainstormstudio/nerd-icons-completion")
   (synopsis "Add icons to completion candidates")
   (description
    "Documentation at https://melpa.org/#/nerd-icons-completion")
   (license #f)))

(define-public emacs-nerd-icons-dired
  (package
   (name "emacs-nerd-icons-dired")
   (version "20241013.212")
   (source
    (origin
     (method git-fetch)
     (uri (git-reference
           (url "https://github.com/rainstormstudio/nerd-icons-dired.git")
           (commit
            "c0b0cda2b92f831d0f764a7e8c0c6728d6a27774")))
     (sha256
      (base32
       "1iwqzh32j6fsx0nl4y337iqkx6prbdv6j83490riraklzywv126a"))))
   (build-system emacs-build-system)
   (propagated-inputs (list emacs-nerd-icons))
   (home-page
    "https://github.com/rainstormstudio/nerd-icons-dired")
   (synopsis
    "Shows icons for each file in dired mode")
   (description
    "Documentation at https://melpa.org/#/nerd-icons-dired")
   (license #f)))

(define-public emacs-nerd-icons-ibuffer
  (package
   (name "emacs-nerd-icons-ibuffer")
   (version "20230417.1549")
   (source
    (origin
     (method git-fetch)
     (uri (git-reference
           (url "https://github.com/seagle0128/nerd-icons-ibuffer.git")
           (commit
            "18c00c03a0d7193bab5e3374ec02c5428db057fd")))
     (sha256
      (base32
       "1wj6kcgvh700maj9i5pmgzc48lbj0dbxx849a8w519m4anr7b23s"))))
   (build-system emacs-build-system)
   (propagated-inputs (list emacs-nerd-icons))
   (home-page
    "https://github.com/seagle0128/nerd-icons-ibuffer")
   (synopsis "Display nerd icons in ibuffer")
   (description
    "Documentation at https://melpa.org/#/nerd-icons-ibuffer")
   (license #f)))

(define-public emacs-ob-async
  (package
   (name "emacs-ob-async")
   (version "1.0.0")
   (source
    (origin
     (method git-fetch)
     (uri (git-reference
	   (url "https://github.com/astahlman/ob-async")
	   ;; (commit (string-append "v" version))))
	   (commit "9aac486073f5c356ada20e716571be33a350a982")))
     (file-name (git-file-name name version))
     (sha256
      (base32 "0k0jcha7cckj8dc2cc1a6m2yhagsl5bmlnr3p8x3g8ij1axk533h"))))
   (build-system emacs-build-system)
   (propagated-inputs
    (list emacs-async emacs-dash))
   (home-page "https://github.com/astahlman/ob-async")
   (synopsis "Asynchronous src_block execution for org-babel")
   (description "@code{ob-async} enables asynchronous execution of org-babel
src blocks.")
   (license license:gpl3+)))

(define-public emacs-indent-bars
  (package
   (name "emacs-indent-bars")
   (version "0.8.2")
   (source
    (origin
     (method git-fetch)
     (uri (git-reference
           (url "https://github.com/jdtsmith/indent-bars.git")
           (commit
            "47ae080d9b7ad03e9097dd064c760e3259fc5d66")))
     (sha256
      (base32
       "11246wn9slm7hl5bn5wyqs4zlsd25v5g24pgzl63v7bbix30v5r2"))))
   (build-system emacs-build-system)
   (propagated-inputs
    (list emacs-compat))
   (home-page
    "https://github.com/jdtsmith/indent-bars")
   (synopsis "Fast, configurable indentation guide-bars for Emac")
   (description
    "Documentation at https://elpa.gnu.org/packages/indent-bars.html")
   (license #f)))

;; Guix version is outdated for julia support
;; (define-public emacs-ess
;;   (let ((commit "0eb240bcb6d0e933615f6cfaa9761b629ddbabdd")
;;         (version "25.01.1")
;;         (revision "1"))
;;     (package
;;      (name "emacs-ess")
;;      (version (git-version version revision commit))
;;      (source
;;       (origin
;;        (method git-fetch)
;;        (uri (git-reference
;;              (url "https://github.com/emacs-ess/ESS")
;;              (commit commit)))
;;        (sha256
;;         (base32 "089kvjjrzas0zsv6wjhmcir62mkbscjpzvqifq0cp58m7wmrbxs2"))
;;        (file-name (git-file-name name version))
;;        (modules '((guix build utils)))
;;        (snippet
;;         #~(begin
;;             ;; Stop ESS from trying to bundle an external julia-mode.el.
;;             (substitute* "lisp/Makefile"
;; 			 ((" \\$\\(JULIAS)") "")
;; 			 (("\ttest.*julia-mode.*\\.el") ""))
;;             ;; Only build docs in info format.
;;             (substitute* "doc/Makefile"
;; 			 (("all  : info text")
;; 			  "all  : info")
;; 			 (("install: install-info install-other-docs")
;; 			  "install: install-info"))
;;             ;; Stop install-info from trying to update the info directory.
;;             (substitute* "doc/Makefile"
;; 			 ((".*/dir.*") ""))
;;             ;; Avoid generating ess-autoloads.el twice.
;;             (substitute* "Makefile"
;; 			 (("all: lisp doc etc autoloads")
;; 			  "all: lisp doc etc"))
;;             ;; Install to correct directories.
;;             (substitute* "Makefile"
;; 			 (("mkdir -p \\$\\(ESSDESTDIR)")
;; 			  "$(MAKE) -C lisp install; $(MAKE) -C doc install")
;; 			 (("\\$\\(INSTALL) -R \\./\\* \\$\\(ESSDESTDIR)/")
;; 			  "$(MAKE) -C etc install"))))))
;;      (build-system gnu-build-system)
;;      (arguments
;;       (let ((base-directory "/share/emacs/site-lisp"))
;;         (list
;;          #:modules '((guix build gnu-build-system)
;;                      (guix build utils)
;;                      (guix build emacs-utils))
;;          #:imported-modules `(,@%default-gnu-imported-modules
;;                               (guix build emacs-build-system)
;;                               (guix build emacs-utils))
;;          #:make-flags
;;          #~(list (string-append "PREFIX=" #$output)
;;                  (string-append "ETCDIR=" #$output #$base-directory "/etc")
;;                  (string-append "LISPDIR=" #$output #$base-directory)
;;                  (string-append "INFODIR=" #$output "/share/info"))
;;          #:phases
;;          #~(modify-phases %standard-phases
;; 			  (delete 'configure)
;; 			  (add-before 'check 'skip-failing-tests
;; 				      (lambda _
;; 					(let-syntax
;; 					    ((disable-tests
;; 					      (syntax-rules ()
;; 						((_ ())
;; 						 (syntax-error "test names list must not be empty"))
;; 						((_ (test-name ...))
;; 						 (substitute* (find-files "test" "\\.el$")
;; 							      (((string-append "^\\(ert-deftest " test-name ".*")
;; 								all)
;; 							       (string-append all "(skip-unless nil)\n"))
;; 							      ...))))
;; 					     (disable-etests  ;different test syntax
;; 					      (syntax-rules ()
;; 						((_ ())
;; 						 (syntax-error "test names list must not be empty"))
;; 						((_ (test-name ...))
;; 						 (for-each
;; 						  (lambda (file)
;; 						    (emacs-batch-edit-file file
;; 									   '(progn
;; 									     (dolist (test (list test-name ...))
;; 										     (goto-char (point-min))
;; 										     (let ((s (format "etest-deftest %s "
;; 												      test)))
;; 										       (when (search-forward s nil t)
;; 											 (beginning-of-line)
;; 											 (kill-sexp))))
;; 									     (basic-save-buffer))))
;; 						  (find-files "test" "\\.el$"))))))
;; 					  (disable-tests ("ess--derive-connection-path"
;; 							  "ess-eval-line-test"
;; 							  "ess-eval-region-test"
;; 							  "ess-mock-remote-process"
;; 							  "ess-r-load-ESSR-github-fetch-no"
;; 							  "ess-r-load-ESSR-github-fetch-yes"
;; 							  "ess-set-working-directory-test"
;; 							  "ess-test-r-startup-directory"))
;; 					  (disable-etests ("ess-r-eval-ns-env-roxy-tracebug-test"
;; 							   "ess-r-eval-sink-freeze-test"
;; 							   ;; Looks like an off-by-one error.
;; 							   "ess--command-browser-unscoped-essr")))))
;; 			  (replace 'check
;; 				   (lambda* (#:key tests? #:allow-other-keys)
;; 				     (when tests? (invoke "make" "test"))))))))
;;      (native-inputs (list perl r-roxygen2 texinfo))
;;      (inputs (list emacs-minimal r-minimal))
;;      (propagated-inputs (list emacs-julia-mode))
;;      (home-page "https://ess.r-project.org/")
;;      (synopsis "Emacs mode for statistical analysis programs")
;;      (description
;;       "Emacs Speaks Statistics (ESS) is an add-on package for GNU Emacs.  It
;; is designed to support editing of scripts and interaction with various
;; statistical analysis programs such as R, Julia, and JAGS.")
;;      (license license:gpl3+))))

(define-public emacs-r-ts-mode
  (package
    (name "emacs-r-ts-mode")
    (version "20241108")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/nverno/r-ts-mode.git")
               (commit
                 "e50b75aec5ab3e630658c581fa678de44ca7a001")))
        (sha256
          (base32
            "0zng1628k2bja9h7r9jzyx0670067xqf9j3fbp74jird3mbqgn39"))))
    (build-system emacs-build-system)
    (propagated-inputs (list emacs-ess))
    (home-page
      "https://github.com/nverno/r-ts-mode")
    (synopsis "R major mode using tree-sitter")
    (description
      "R major mode using tree-sitter")
    (license #f)))

(define-public emacs-eshell-vterm
  (package
    (name "emacs-eshell-vterm")
    (version "20240305.1149")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/iostapyshyn/eshell-vterm.git")
               (commit
                 "20f4b246fa605a1533cdfbe3cb7faf31a24e3d2e")))
        (sha256
          (base32
            "1akqxmgq8838v5nmxdldcgpvr3qf3qz64qsfnf90ic4mivdjgffy"))))
    (build-system emacs-build-system)
    (propagated-inputs (list emacs-vterm))
    (home-page
      "https://github.com/iostapyshyn/eshell-vterm")
    (synopsis "Vterm for visual commands in eshell")
    (description
      "Documentation at https://melpa.org/#/eshell-vterm")
    (license #f)))

(define-public emacs-julia-vterm
  (package
   (name "emacs-julia-vterm")
   (version "20240514.724")
   (source
    (origin
     (method git-fetch)
     (uri (git-reference
           (url "https://github.com/shg/julia-vterm.el.git")
           (commit
            "2298cd42d354f069adbb7bb06b3b15222e5f54a2")))
     (sha256
      (base32
       "0r0anwzar8rwiwzwg637nshj59mssiipbilcc6kvxr73ffviy127"))))
   (build-system emacs-build-system)
   (propagated-inputs (list emacs-vterm))
   (home-page
    "https://github.com/shg/julia-vterm.el")
   (synopsis "A mode for Julia REPL using vterm")
   (description
    "Documentation at https://melpa.org/#/julia-vterm")
   (license #f)))

;; see https://github.com/shg/ob-julia-vterm.el/pull/28
(define-public emacs-ob-julia-vterm
  (package
   (name "emacs-ob-julia-vterm")
   (version "20240825")
   (source
    (origin
     (method git-fetch)
     (uri (git-reference
           (url "https://github.com/stuartthomas25/ob-julia-vterm.el.git")
           ;; (commit
           ;; "e1aae4f54cd06f33c63a16d88df4856947f46201")))
           (commit
            "98f201122684405449996980fc77dd4c1360c228")))
     (sha256
      (base32
       "0my76d65yjgrj8ckna8d4vkxc7n09my6fq7165ag8wbzl7s1cqqg"))))
   (build-system emacs-build-system)
   (propagated-inputs
    (list emacs-julia-vterm emacs-queue))
   (home-page
    "https://github.com/stuartthomas25/ob-julia-vterm.el")
   (synopsis
    "Babel functions for Julia that work with julia-vterm")
   (description
    "Documentation at https://melpa.org/#/ob-julia-vterm")
   (license #f)))

(define-public emacs-julia-ts-mode
  (package
   (name "emacs-julia-ts-mode")
   (version "20250115")
   (source
    (origin
     (method git-fetch)
     (uri (git-reference
	   (url "https://github.com/JuliaEditorSupport/julia-ts-mode.git")
	   ;; (url "https://github.com/ronisbr/julia-ts-mode.git") ;; broken for now... see https://github.com/JuliaEditorSupport/julia-ts-mode/issues/25
	   ;; (url "https://github.com/dhanak/julia-ts-mode")
           (commit
	    "d693c6b35d3aed986b2700a3b5f910de12d6c53c"
            ;; "758004341a3f7bde2927401af5afbad67594fa8c"
            ;; "c60a900b731b5ea3ea2f0b4f2c01994ee81259d4"
	    )))
     (sha256
      (base32
       "0jg59d6q4lab2p5d2f8yp95xpbqkc614ayl137mv14l5apgayvbc"
       ;; "1jdqy463aisklj72nj3j0q8ck5gdws3j8r0y2fd7yqcjmslcblrh"
       ;; "0596avkfr4dydk72dmy37ykggky3j2x23lv9jswn8ndf8sr6hqyq"
))))
   (build-system emacs-build-system)
   (propagated-inputs (list emacs-julia-mode))
   ;; (home-page "https://github.com/ronisbr/julia-ts-mode")
   (home-page "https://github.com/JuliaEditorSupport/julia-ts-mode")
   (synopsis
    "Julia major mode using tree-sitter")
   (description
    "Julia major mode using tree-sitter")
   (license #f)))


(define-public emacs-python-vterm
  (package
   (name "emacs-python-vterm")
   (version "20241209")
   (source
    (origin
     (method git-fetch)
     (uri (git-reference
           (url "https://github.com/shg/python-vterm.el.git")
           (commit
            "aa0395688dafcc134daf6529bd5985010e3d09c0")))
     (sha256
      (base32
       "0q32y4ly9j3j3pqi2jli004xjabif1jjvldvbjxjmawai48fvbqx"))))
   (build-system emacs-build-system)
   (propagated-inputs (list emacs-vterm))
   (home-page
    "https://github.com/shg/python-vterm.el")
   (synopsis " A simple vterm-based mode for an inferior Python REPL process in Emacs ")
   (description
    "Documentation at https://github.com/shg/python-vterm.el")
   (license #f)))

(define-public emacs-ob-python-vterm
  (package
   (name "emacs-ob-python-vterm")
   (version "20241218")
   (source
    (origin
     (method git-fetch)
     (uri (git-reference
           (url "https://github.com/shg/ob-python-vterm.el.git")
           (commit
            "05acc26a7240f96436c968a27df8c56051401045")))
     (sha256
      (base32
       "0jg3gmmah4d34i7lf4rh15fs3n4fxs82s36dw0llp7ydp9lna010"))))
   (build-system emacs-build-system)
   (propagated-inputs
    (list emacs-python-vterm emacs-queue))
   (home-page
    "https://github.com/shg/ob-python-vterm.el")
   (synopsis
    "Babel functions for Julia that work with julia-vterm")
   (description
    "Documentation at https://github.com/shg/ob-python-vterm.el")
   (license #f)))

(define-public emacs-org-sliced-images
  (package
   (name "emacs-org-sliced-images")
   (version "20240624.428")
   (source
    (origin
     (method git-fetch)
     (uri (git-reference
           (url "https://github.com/jcfk/org-sliced-images.git")
           (commit
            "b98b88a55eff07e998e7789e0bf7307dd71db050")))
     (sha256
      (base32
       "0iq03zp3bm1ph5ryhx6zpjm830sliqj6bb7i0h2v0nfn07l0cby2"))))
   (build-system emacs-build-system)
   (home-page
    "https://github.com/jcfk/org-sliced-images")
   (synopsis "Sliced inline images in org-mode")
   (description
    "Documentation at https://melpa.org/#/org-sliced-images")
   (license #f)))

(define-public emacs-ultra-scroll
  (package
   (name "emacs-ultra-scroll")
   (version "20250113.64ad7be")
   (source
    (origin
     (method git-fetch)
     (uri (git-reference
           (url "https://github.com/jdtsmith/ultra-scroll.git")
           (commit
            "64ad7be02e11317576498dabb15c92cf31e2c04c")))
     (sha256
      (base32
       "0wr6blgdgis3xf52km0jdxacnbi76v689yqg75jqfcb8581z8z4l"))))
   (build-system emacs-build-system)
   (home-page
    "https://github.com/jdtsmith/ultra-scroll")
   (synopsis "scroll Emacs like lightning")
   (description
    "scroll Emacs like lightning")
   (license #f)))



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Updated packages 
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(define emacs-multi-vterm-upd
  (package
   (inherit emacs-multi-vterm)
   (propagated-inputs (modify-inputs (package-propagated-inputs emacs-multi-vterm)
				     (replace "emacs-vterm" emacs-vterm)
			))))


(define emacs-magit-todos-upd
  (package
   (inherit emacs-magit-todos)
   (propagated-inputs (modify-inputs (package-propagated-inputs emacs-magit-todos)
				     (replace "emacs-magit" emacs-magit)
			))))

(define emacs-forge-upd
  (package
   (inherit emacs-forge)
   (propagated-inputs (modify-inputs (package-propagated-inputs emacs-forge)
				     (replace "emacs-magit" emacs-magit)
			))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Packages list 
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(define %emacs-packages
  (list
   (specification->package "emacs-next-pgtk") ;; Emacs text editor with `pgtk' frames
   (specification->package "emacs-benchmark-init") ;; Benchmark Emacs `require' and `load' calls
   ))

(define %modal-editing-packages
  (list
   (specification->package "emacs-evil") ;; Extensible Vi layer for Emacs
   (specification->package "emacs-evil-collection") ;; Collection of Evil bindings for many major and minor modes
   (specification->package "emacs-evil-org") ;; Evil keybindings for Org mode
   ))

(define %project-packages
  (list
   (specification->package "emacs-dired-sidebar") ;; Sidebar for Emacs using Dired 
   ;; emacs-ibuffer-sidebar
   ))

(define %icons-packages
  (list
   (specification->package "emacs-nerd-icons") ;;  
   emacs-nerd-icons-completion
   emacs-nerd-icons-dired
   emacs-nerd-icons-ibuffer
   ;; emacs-magit-file-icons
   ))

(define %terminal-packages
  (list
   ;; (specification->package "emacs-coterm") ;; Terminal emulation for comint  
   ;; (specification->package "emacs-vterm") ;; Emacs libvterm integration  
   ;; (specification->package "emacs-multi-vterm") ;; Manage multiple vterm buffers in Emacs  
   emacs-eshell-vterm
   emacs-vterm
   emacs-multi-vterm-upd
   ))

(define %utilities-packages
  (list
   (specification->package "emacs-guix") ;; Emacs interface for GNU Guix  
   ))

(define %browser-packages
  (list
   (specification->package "emacs-nyxt") ;; Interact with Nyxt from Emacs  
   ))

(define %torrents-packages
  (list
   (specification->package "emacs-transmission") ;; Emacs interface to a Transmission session  
   ))

(define %r-packages
  (list
   (specification->package "emacs-ess") ;; Emacs mode for statistical analysis programs  
   ;; emacs-ess ;; Emacs mode for statistical analysis programs  
   emacs-r-ts-mode
   ))

(define %julia-packages
  (list
   ;; (specification->package "emacs-ess") ;; Emacs mode for statistical analysis programs  
   ;; (specification->package "emacs-julia-mode") ;; Major mode for Julia  
   ;; (specification->package "emacs-spinner") ;; Emacs mode-line spinner for operations in progress  
   ;; (specification->package "emacs-julia-snail") ;; Development environment and REPL interaction package for Julia  
   emacs-julia-ts-mode
   emacs-julia-vterm
   emacs-ob-julia-vterm
   emacs-lsp-julia
   ))

(define %python-packages
  (list
   emacs-python-vterm
   emacs-ob-python-vterm
   ))

(define %notebook-packages
  (list
   ;; (specification->package "emacs-ob-async") ;; Asynchronous src_block execution for org-babel  
   emacs-ob-async
   ;; (specification->package "emacs-jupyter") ;; Emacs interface to communicate with Jupyter kernels
   ))

(define %hightlight-packages
  (list
   ;; (specification->package "emacs-lin") ;; Make Hl Line mode more suitable for selection UIs
   ;; (specification->package "emacs-hl-todo") ;; Emacs mode to highlight TODO and similar keywords
   ;; (specification->package "emacs-rainbow-delimiters") ;; Highlight brackets according to their depth
   (specification->package "emacs-rainbow-mode") ;; Colorize color names in buffers
   ;; (specification->package "emacs-diff-hl") ;; Highlight uncommitted changes using VC
   ;; (specification->package "emacs-highlight-indent-guides") ;; Minor mode to highlight indentation
   emacs-indent-bars
   emacs-highlight-indent-guides
   ))

(define %todos-packages
  (list
   (specification->package "emacs-hl-todo") ;; Emacs mode to highlight TODO and similar keywords
   emacs-consult-todo
   ))

(define %calendar-packages
  (list
   (specification->package "emacs-org-caldav") ;; Sync Org files with external calendars via the CalDAV protocol
   ))

(define %pdf-packages
  (list
   (specification->package "emacs-pdf-tools") ;; Emacs support library for PDF files
   ))

(define %password-packages
  (list
   (specification->package "emacs-pass") ;; Major mode for `password-store.el'
   ;; (specification->package "emacs-password-store") ;; Password store (pass) support for Emacs
   (specification->package "emacs-password-store-otp") ;; Interact with the `pass-otp' extension for `pass' from Emacs
   ;; (specification->package "emacs-password-generator") ;; Generate passwords inside Emacs
   ))


(define %scrolling-packages
  (list
   emacs-ultra-scroll
   ))

(define %themes-packages
  (list
   ;; (specification->package "emacs-gruvbox-theme") ;; Gruvbox is a retro groove color scheme ported from Vim
   ;; (specification->package "emacs-monokai-theme") ;; High contrast color theme for Emacs
   (specification->package "emacs-doom-themes") ;; Wide collection of color themes for Emacs
   (specification->package "emacs-doom-modeline") ;; Fancy and fast mode-line inspired by minimalism design
   ))

(define %completion-packages
(specifications->packages
 (list
  "emacs-vertico" ;; Vertical interactive completion  
  "emacs-orderless" ;; Emacs completion style that matches multiple regexps in any order  
  "emacs-marginalia" ;; Marginalia in the minibuffer completions  
  "emacs-consult" ;; Consulting completing-read
  "emacs-embark" ;; Emacs mini-buffer actions rooted in keymaps
  "emacs-cape" ;; Completion at point extensions for Emacs
)))


(define %windows-packages
  (list
   (specification->package "emacs-ace-window") ;; Quickly switch windows in Emacs
   ))

(define %markdown-packages
  (specifications->packages
   (list
    "emacs-markdown-mode" ;; Emacs Major mode for Markdown files
    "emacs-toc-org" ;; Table of Contents generator for Emacs Org mode
    )))

(define %bibliography-alt-packages
  (list
   (specification->package "emacs-biblio") ;; Browse and import bibliographic references
   (specification->package "emacs-citar") ;; Emacs package to quickly find and act on bibliographic entries
   ))

(define %chatbot-packages
  (list
   (specification->package "emacs-gptel") ;; GPTel is a simple ChatGPT client for Emacs
   ))

(define %tabs-packages
  (list
   ;; (specification->package "emacs-project-tab-groups") ;; Support a "one tab group per project" workflow
   emacs-tab-bar-echo-area
   ))

(define %emails-packages
  (specifications->packages
   (list
    "emacs-notmuch"    ;; Run Notmuch within Emacs  
    "emacs-notmuch-maildir" ;; Visualize maildirs as a tree
    "emacs-notmuch-indicator" ;; Display a mode line indicator with `notmuch-count' output
    "emacs-consult-notmuch" ;; Search and preview Notmuch emails using Consult
    "emacs-ol-notmuch" ;; Links to notmuch messages for Emacs' Org mode
    )))

(define %bluetooth-packages
  (list
   (specification->package "emacs-bluetooth") ;; Manage Bluetooth devices using Emacs
   ))

(define %multimedia-packages
  (list
   ;; (specification->package "emacs-mpdel") ;; Emacs user interface for Music Player Daemon (MPD)
   ;; (specification->package "emacs-simple-mpc") ;; Simple Emacs frontend to mpc
   (specification->package "emacs-emms") ;; The Emacs Multimedia System
   ;; (specification->package "emacs-emms-mode-line-cycle") ;; Display the EMMS mode line as a ticker
   ))

(define %keybindings-packages
  (list
   ;; (specification->package "emacs-use-package") ;; Declaration for simplifying your .emacs
   (specification->package "emacs-general") ;; More convenient key definitions in emacs
   emacs-use-package
   ))

(define %container-packages
  (list
   (specification->package "emacs-docker") ;; Manage docker from Emacs
   ))

(define %modeline-packages
  (list
   emacs-echo-bar
   ))

(define %desktop-packages
  (list
   (specification->package "emacs-app-launcher") ;; Use Emacs standard completion to launch applications
   ))

(define %csv-packages
  (list
   (specification->package "emacs-csv-mode") ;; Major mode for editing comma/char separated values
   ))

(define %corrector-packages
  (list
   (specification->package "emacs-langtool") ;; Major mode for editing comma/char separated values
   emacs-lsp-ltex
   emacs-eglot-ltex
   emacs-flymake-languagetool
   ))

(define %file-explorer-packages
  (list
   ;; (specification->package "emacs-dirvish") ;; Improved version of the Emacs package Dired
   ;; emacs-dirvish ;; Improved version of the Emacs package Dired
   ;; (specification->package "fd") ;; Simple, fast and user-friendly alternative to find
   ;; (specification->package "imagemagick") ;; Create, edit, compose, or convert bitmap images
   ;; (specification->package "poppler") ;; PDF rendering library
   ;; (specification->package "ffmpegthumbnailer") ;; Create thumbnails from video files
   ;; (specification->package "mediainfo") ;; Utility for reading media metadata
   ;; (specification->package "tar") ;; Managing tar archives
   ;; (specification->package "unzip") ;; Decompression and file extraction utility
   ))


(define %org-packages
  (append
   (specifications->packages
    (list
   "emacs-org-appear" ;; Make invisible parts of Org fragments appear visible  
   "emacs-org-superstar" ;; Prettify headings and plain lists in Org mode  
   "emacs-org-download" ;; Facilitate moving images  
   "emacs-toc-org" ;; Table of Contents generator for Emacs Org mode
   ;; "emacs-org-modern" ;; Modern Org style
   ;; "emacs-org-fragtog" ;; Toggle Org mode LaTeX fragments preview at cursor  
   ))
   (list
   emacs-org-sliced-images
   )))

(define %lsp-packages
  (specifications->packages
   (list
    "emacs-eglot" ;; Client for Language Server Protocol (LSP) servers
    "emacs-eglot-booster" ;; Configuration to use LSP-booster with Eglot
    "emacs-lsp-booster" ;; Emacs LSP performance booster
    "emacs-lsp-mode" ;; Emacs client and library for the Language Server Protocol
    "emacs-lsp-ui" ;; User interface extensions for `lsp-mode'
    )))

(define %social-packages
  (list
   (specification->package "emacs-mastodon") ;; Emacs client for Mastodon
   ))

(define %version-control-packages
  (append
   (specifications->packages
    (list
     ;; "emacs-magit"
     ;; "emacs-magit-todos"
     ;; "emacs-forge"
     ))
   (list
    emacs-magit
    emacs-magit-todos-upd
    emacs-forge-upd
    )))

(define %blog-packages
  (list
   emacs-org2blog
   ))

(define %haskell-packages
  (specifications->packages
   (list
    "emacs-haskell-mode" ;; Haskell mode for Emacs
    "emacs-haskell-snippets" ;; Official collection of YASnippet Haskell snippets for Emacs
    )))

(define %snippets-packages
  (specifications->packages
   (list
    "emacs-yasnippet" ;; Yet another snippet extension for Emacs
    "emacs-yasnippet-snippets" ;; Collection of YASnippet snippets for many languages
    )))



(define %checker-packages
  (specifications->packages
   (list
    ;; "emacs-flymake" ;; Universal on-the-fly syntax checker
    "emacs-flymake-collection" ;; Collection of checkers for Flymake
    )))

(define %bookmarks-packages
  (list
   emacs-tab-bookmark
   ))

(define %notes-packages
  (specifications->packages
   (list
    "emacs-denote" ;; Simple notes for Emacs
    "emacs-denote-menu" ;; View and filter Denote files in a tabulated list  
    "emacs-denote-explore" ;; Analyse and visualise a collection of Denote notes
   )))

;; Manifest
(packages->manifest
 (append
  %bibliography-alt-packages
  %browser-packages
  %calendar-packages
  %chatbot-packages
  %completion-packages
  %emacs-packages
  %hightlight-packages
  %icons-packages
  %julia-packages
  %emails-packages
  %markdown-packages
  %modal-editing-packages
  %notebook-packages
  %org-packages
  %password-packages
  %pdf-packages
  %project-packages
  %python-packages
  %r-packages
  %terminal-packages
  %themes-packages
  %torrents-packages
  %utilities-packages
  %bluetooth-packages
  %multimedia-packages
  %keybindings-packages
  %todos-packages
  %container-packages
  %modeline-packages
  ;; %evil-packages
  %scrolling-packages
  %tabs-packages
  %desktop-packages
  %csv-packages
  %corrector-packages
  %file-explorer-packages
  %org-packages
  %social-packages
  %version-control-packages
  %blog-packages
  %haskell-packages
  %snippets-packages
  %checker-packages
  %lsp-packages
  %bookmarks-packages
  %notes-packages
  ;; %windows-packages
  ))
