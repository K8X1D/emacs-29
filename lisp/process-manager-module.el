;;; .* --- .*"  -*- lexical-binding: t; -*-
;;; Commentary:
;;; Code:

(use-package proced
  :general
  (k8x1d/leader-key
   "oP"  '(proced :which-key "Process manager")
   )
  :bind
  (:map k8x1d/leader-map
	("oP" ("Process manager" . proced))
	)
  :config
  (setq proced-auto-update-interval 1)
  (setq proced-auto-update-flag t)
  (setq proced-enable-color-flag t)
  )

(provide 'process-manager-module)
;;; process-manager-module.el ends here
