;;; .* --- .*"  -*- lexical-binding: t; -*-
;;; Commentary:
;;; Code:

;; Docker management
;; Don't work with podman
(use-package docker
  :disabled
  :general
  (k8x1d/leader-key
   "od"  '(docker :which-key "Docker")
   )
  )

(provide 'container-module)
;;; container-module.el ends here
