;; see https://github.com/daviwil/emacs-from-scratch/blob/master/hallo-theme.el
(require 'autothemer)
(autothemer-deftheme k8x1d "K8X1D emacs theme."

  ;; Specify the color classes used by the theme
  ((((class color) (min-colors #xFFFFFF))
    ((class color) (min-colors #xFF)))

   ;; Specify the color palette, color columns correspond to each of the classes above.
   (k8x1d-red "#fb4934" "#cc241d")
   (k8x1d-green "#b8bb26" "#98971a")
   (k8x1d-yellow "#fabd2f" "#d79921")
   (k8x1d-blue "#83a598" "#458588")
   (k8x1d-purple "#d3869b" "#b16286")
   (k8x1d-aqua "#8ec07c" "#689d6a")
   (k8x1d-orange "#fe8019" "#d65d0e")
   (k8x1d-gray "#a89984" "#928374")

   (k8x1d-fg "#ebdbb2" "#ebdbb2")
   (k8x1d-fg-s "#fbf1c7" "#fbf1c7")
   (k8x1d-fg-h "#d5c4a1" "#d5c4a1")

   (k8x1d-bg "#282828" "#282828")
   (k8x1d-bg-s "#3c3836" "#3c3836")
   (k8x1d-bg-h "#1d2021" "#1d2021"))

    ;; Specifications for Emacs faces.
    ;; Simpler than deftheme, just specify a face name and 
    ;; a plist of face definitions (nested for :underline, :box etc.)
  (
   ;; Base
   (default (:foreground k8x1d-fg :background k8x1d-bg))
   (cursor (:foreground k8x1d-bg :background k8x1d-fg))
   (mode-line (:foreground k8x1d-fg-s :background k8x1d-bg-h :box nil))
   (mode-line-inactive (:foreground k8x1d-fg-h :background k8x1d-bg-s :box nil))
     ;; (fringe                (:background gruvbox-bg))
     (highlight (:background k8x1d-bg-s))
     ;; (region                (:background gruvbox-dark2)) ;;selection
     ;; (secondary-selection   (:background gruvbox-dark1))
     ;; (minibuffer-prompt     (:foreground gruvbox-bright_green :bold t))
     ;; (vertical-border       (:foreground gruvbox-dark2))
     ;; (internal-border       (:background gruvbox-dark2))
     ;; (window-divider        (:foreground gruvbox-dark2))
     ;; (link                  (:foreground gruvbox-faded_blue :underline t))
     ;; (shadow                (:foreground gruvbox-dark4))


     ;;; Built-in syntax

     (font-lock-builtin-face                            (:foreground k8x1d-orange))
     (font-lock-constant-face                           (:foreground k8x1d-purple))
     (font-lock-comment-face                            (:foreground k8x1d-gray))
     (font-lock-function-name-face                      (:foreground k8x1d-yellow))
     (font-lock-keyword-face                            (:foreground k8x1d-red))
     (font-lock-string-face                             (:foreground k8x1d-green))
     (font-lock-variable-name-face                      (:foreground k8x1d-aqua))
     (font-lock-type-face                               (:foreground k8x1d-purple))
     (font-lock-warning-face                            (:foreground k8x1d-red :bold t))

     ;;; tab-bar
     (tab-bar-tab-inactive (:foreground k8x1d-fg :background k8x1d-bg-s))
     (tab-bar-tab (:foreground k8x1d-fg :background k8x1d-bg-h))
     (tab-bar (:foreground k8x1d-fg :background k8x1d-bg))
   
     )

    ;; Forms after the face specifications are evaluated.
    ;; (palette vars can be used, read below for details.)
    
    (custom-theme-set-variables 'k8x1d
        `(ansi-color-names-vector [,k8x1d-red
                                   ,k8x1d-green
                                   ,k8x1d-blue
                                   ,k8x1d-purple
                                   ,k8x1d-yellow
                                   ,k8x1d-orange
                                   ,k8x1d-aqua])))

(provide 'k8x1d)
