;;; .* --- .*"  -*- lexical-binding: t; -*-
;;; Commentary:
;;; Code:

;; (use-package vc-hooks
;;   :ensure nil
;;   :init
;;   (setq vc-follow-symlinks t)
;;   )

;; (use-package files
;;   :ensure nil
;;   :init
;;   (setq view-read-only t)
;;   )

(use-package magit
  :general
  (k8x1d/leader-key
    "g" '(:ignore t :which-key "Git")
    "gg" '(magit-status :which-key "Status")
    )
  :bind
  (([remap magit-jump-to-tracked] . tab-bar-switch-to-next-tab)
   ;; ("C-c g g" . magit-status)
   )
  :config
  (setopt magit-format-file-function #'magit-format-file-nerd-icons)
  )

;; Show todos in magit panel
(use-package magit-todos
  :hook
  (magit-mode . magit-todos-mode)
  )

;; Issue interaction
(use-package forge
  :general
  (k8x1d/leader-key
    "gi" '(:ignore t :which-key "Issues")
    "gic" '(forge-create-issue :which-key "Create")
    "gil" '(forge-list-issues :which-key "List")
    "gia" '(forge-add-repository :which-key "Add repository")
    )
  :bind
  ;; ("C-c g c i" . forge-create-issue)
  ;; ("C-c g l i" . forge-list-issues)
  (:map forge-issue-mode-map
	("g t" . tab-bar-switch-to-next-tab)
	)
  :config
  ;; Remove flyspell, using ltex instead
  (remove-hook 'forge-post-mode-hook #'turn-on-flyspell)

  ;; add bin/phun gitlan
  (add-to-list 'forge-alist
               `("gitlab.cirst.ca" "gitlab.cirst.ca/api/v4"
                 "gitlab.cirst.ca" forge-gitlab-repository))
  (setq forge-add-default-bindings nil)
  )

(provide 'version-control-module)
;;; version-control-module.el ends here
