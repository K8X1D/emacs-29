;;; .* --- .*"  -*- lexical-binding: t; -*-
;;; Commentary:
;;; Code:

(use-package bluetooth
  :general
  (k8x1d/leader-key
   "oB"  '(bluetooth-list-devices :which-key "Bluetooth")
   )
  :bind
  (:map k8x1d/leader-map
	("oB" ("Bluetooth" . bluetooth-list-devices))
	)
  )

(provide 'bluetooth-module)
;;; bluetooth-module.el ends here

