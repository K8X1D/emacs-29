;;; package --- Summary  -*- lexical-binding: t; -*-
;;; Commentary:
;;; Code:

;; TODO: exclude org-mode
(use-package hl-todo
  :hook
  (after-init . global-hl-todo-mode))

(use-package consult-todo
  :config
  (require 'project)
  ;; Add todo list to project command
  (add-to-list 'project-switch-commands '(consult-todo-project "Todos" "T"))
  )


(provide 'todos-module)
;;; todos-module.el ends here
