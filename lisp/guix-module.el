;;; .* --- .*"  -*- lexical-binding: t; -*-
;;; Commentary:
;;; Code:

(use-package guix
  :general
  (k8x1d/leader-key
   "og"  '(guix :which-key "Guix")
   )
  :bind
  (:map k8x1d/leader-map
	("og" ("Guix" . guix))
	)
  :config
  (setq guix-pulled-profile "/home/k8x1d/.config/guix/current")
  )

(provide 'guix-module)
;;; guix-module.el ends here
