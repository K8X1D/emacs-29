;; .* --- .*"  -*- lexical-binding: t; -*-
;;; Commentary:
;;; Code:

;; Garbage collection management optimization
(setq gc-cons-threshold most-positive-fixnum) ;; Increase gc threshold at start, speed start up
(run-with-idle-timer 5 t (lambda ()
			   (garbage-collect))) ;; Garbage collect when idle
(add-hook 'emacs-startup-hook (lambda ()
				(setq gc-cons-threshold  67108864) ;; 64M
				)) ;; Set gc threshold


;; Reduce logs
(setq byte-compile-warnings '(not obsolete))
(setq warning-suppress-log-types '((comp) (bytecomp)))
(setq native-comp-async-report-warnings-errors 'silent)

(provide 'optimisations-module)
;;; optimisations-module.el ends here
