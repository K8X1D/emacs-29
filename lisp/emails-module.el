;;; .* --- .*"  -*- lexical-binding: t; -*-
;;; Commentary:
;;; Code:

(use-package notmuch
  :preface
  (defun k8x1d/open-emails-tab ()
    (interactive)
    (k8x1d/create-or-switch-tab-then "Emails" #'notmuch)
    )
  :general
  (k8x1d/leader-key
    "oe" '(k8x1d/open-emails-tab :which-key "Emails")
    )
  :bind
  (:map k8x1d/leader-map
	("oe" ("Email" . notmuch))
	)
  :config
  (setq notmuch-show-logo nil)
  (setq notmuch-hello-auto-refresh t)

  ;; (setq mail-signature-file t)
  (setq message-signature t)
  ;; (setq message-signature-directory user-emacs-directory)
  (setq message-signature-file (concat user-emacs-directory ".signature"))
  ;; (setq mail-signature-file )

  (setq notmuch-saved-searches '((:name "inbox" :query "tag:inbox" :key "i")
				 (:name "unread" :query "tag:unread" :key "u")
				 (:name "flagged" :query "tag:flagged" :key "f")
				 (:name "sent" :query "tag:sent" :key "t")
				 (:name "drafts" :query "tag:draft" :key "d")
				 (:name "all mail" :query "*" :key "a")
				 (:name "pedalo" :query "tag:pedalo"))
	)

  ;; Functions
  ;; from https://notmuchmail.org/emacstips/#index12h2
  (defun my-notmuch-mua-empty-subject-check ()
    "Request confirmation before sending a message with empty subject"
    (when (and (null (message-field-value "Subject"))
	       (not (y-or-n-p "Subject is empty, send anyway? ")))
      (error "Sending message cancelled: empty subject.")))
  (add-hook 'message-send-hook 'my-notmuch-mua-empty-subject-check)
  )

;; Identification
(use-package emacs
  :config
  (setq user-mail-address "K8X1D@proton.me")
  (setq user-full-name  "Kevin Kaiser")
  )

;; Send through smtp
(use-package smtpmail
  :config
  (setq send-mail-function 'smtpmail-send-it)
  (setq message-send-mail-function 'smtpmail-send-it)
  (setq smtpmail-auth-credentials "~/.authinfo.gpg") ;; Here I assume you encrypted the credentials
  (setq smtpmail-smtp-server "127.0.0.1")
  (setq smtpmail-smtp-service 1025)
  )

;; Modeline indicator
(use-package notmuch-indicator
  :hook
  (after-init . notmuch-indicator-mode)
  :config
  (setq notmuch-indicator-hide-empty-counters t)
  (setq notmuch-indicator-refresh-count 30)
  ;; (setq notmuch-indicator-add-to-mode-line-misc-info nil) ;; done manually
  (setq notmuch-indicator-add-to-mode-line-misc-info t) ;; for doom-modeline
  ;; (setq notmuch-indicator-args '((:terms "tag:unread and tag:inbox" :label "󰇮")))
  ;; (setq notmuch-indicator-args '((:terms "tag:unread and tag:inbox")))
  (setq notmuch-indicator-args
	'((:terms "tag:unread and tag:inbox" :label "󰇮 ")
	  (:terms "tag:flagged" :label "󰀨 ")
	  ;; (:terms "from:pedalo" :label " ")
	  ;; (:terms "from:yannis" :label "󰥱 ")
	  ))

  (setq notmuch-indicator-counter-format "[%s%s]")
  ;; (add-to-list 'global-mode-string '(notmuch-indicator-mode ("(" (:eval notmuch-indicator--counters) ")")))
  ;; (add-to-list 'global-mode-string '(notmuch-indicator-mode notmuch-indicator-mode-line-construct))
  )

;; Show maildirs as tree
(use-package notmuch-maildir
  :hook
  (notmuch-hello-mode . notmuch-maildir-inject-section)
  )

;; Enable org-store-link for notmuch
(use-package ol-notmuch)

;; Consult integration
(use-package consult-notmuch)


(provide 'emails-module)
;;; emails-module.el ends here
