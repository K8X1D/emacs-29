;;; .* --- .*"  -*- lexical-binding: t; -*-
;;; Commentary:
;;; Code:

;; Show line number when coding
(use-package display-line-numbers
  :hook
  (prog-mode . display-line-numbers-mode)
  (bibtex-mode . display-line-numbers-mode)
  :config
  (setq display-line-numbers-type 'relative)
  )





(provide 'line-numbers-module)
;;; line-numbers-module.el ends here
