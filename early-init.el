;;; .* --- .*"  -*- lexical-binding: t; -*-

;; Load path
(add-to-list 'load-path (concat user-emacs-directory "lisp"))
;; (setq custom-theme-directory "/home/k8x1d/.config/emacs-minimal/")
(add-to-list 'custom-theme-load-path (concat user-emacs-directory "themes/"))
(add-to-list 'custom-theme-load-path (concat user-emacs-directory "local-packages/"))
;; (setq custom-theme-directory (concat user-emacs-directory "themes/"))

;; Simplify ui
(tool-bar-mode -1)             ; Hide the outdated icons
(scroll-bar-mode -1)           ; Hide the always-visible scrollbar
(setq inhibit-splash-screen t) ; Remove the "Welcome to GNU Emacs" splash screen
(setq use-file-dialog nil)     ; Ask for textual confirmation instead of GUI
(setq use-dialog-box nil)      ; Remove dialog box
(setq use-short-answers t)     ; y-or-n insteat of yes-or-no
(push '(menu-bar-lines . 0) default-frame-alist) ; Remove ui element from default-frame
(push '(tool-bar-lines . 0) default-frame-alist) ; Remove ui element from default-frame
(push '(vertical-scroll-bars) default-frame-alist) ; Remove ui element from default-frame
(add-to-list 'default-frame-alist '(undecorated . t)) ;; river workaround https://codeberg.org/river/wiki#woraround-for-emacs

;; Packages
(setq package-enable-at-startup nil) ;; Disable package.el by default

;; Garbage collection management optimization
(setq gc-cons-threshold most-positive-fixnum) ;; Increase gc threshold at start, speed start up
(run-with-idle-timer 5 t (lambda ()
			   (setq gc-cons-threshold  67108864) ; 64M or whatever value you like
			   (garbage-collect))) ;; Garbage collect when idle

;; Backup files management
(setq create-lockfiles nil)  ;; Avoid generating lockfile
;; (setq make-backup-files nil) ;; Avoid generating backups
(setq backup-directory-alist
      `(("." . ,(expand-file-name "backup" "~/.config/emacs"))))
(setq tramp-backup-directory-alist backup-directory-alist)
(setq backup-by-copying-when-linked t)
(setq backup-by-copying t)  ; Backup by copying rather renaming
(setq delete-old-versions t)  ; Delete excess backup versions silently
(setq version-control t)  ; Use version numbers for backup files
(setq kept-new-versions 5)
(setq kept-old-versions 5)
(setq vc-make-backup-files nil)  ; Do not backup version controlled files


;; Custom file management
(setq custom-file (expand-file-name "customs.el" user-emacs-directory))
(unless (file-exists-p custom-file)
  (write-region "" nil custom-file))
(load custom-file nil t) ; Load custom file. Don't hide errors. Hide success message

;; Byte-compile
(setq byte-compile-warnings nil) ;; silence warning
(setq byte-compile-debug nil) ;; silence debug

;; Native compilation
(setq native-comp-async-report-warnings-errors 'silent) ;; silence message

;; Log
(setq warning-minimum-log-level :error)

;; Optimizations
;; from minimal emacs https://github.com/jamescherti/minimal-emacs.d
(setq load-prefer-newer t) ;; Prefer loading newer compiled files
;; See https://www.reddit.com/r/emacs/comments/1b25904/is_there_anything_i_can_do_to_make_eglots/
(setq read-process-output-max (* 1024 1024))  ;; Increase how much is read from processes in a single chunk (default is 4kb).
(setq process-adaptive-read-buffering nil)
;; Reduce rendering/line scan work by not rendering cursors or regions in non-focused windows.
(setq-default cursor-in-non-selected-windows nil)
(setq highlight-nonselected-windows nil)
;; Disable warnings from the legacy advice API. They aren't useful.
(setq ad-redefinition-action 'accept)
(setq warning-suppress-types '((lexical-binding)))
(setq ffap-machine-p-known 'reject) ;; Don't ping things that look like domain names.
(setq idle-update-delay 1.0) ;; By default, Emacs "updates" its ui more often than it needs to
;; Font compacting can be very resource-intensive, especially when rendering
;; icon fonts on Windows. This will increase memory usage.
(setq inhibit-compacting-font-caches t)

;; Prevent inital white flash
;; (add-to-list 'default-frame-alist '(foreground-color . "#ebdbb2"))
;; (add-to-list 'default-frame-alist '(background-color . "#282828"))

;; Transparency (for gtk)
(set-frame-parameter nil 'alpha-background 80) ;; initial transparency For current frame
(add-to-list 'default-frame-alist '(alpha-background . 80)) ;; initial transparency for all new frames henceforth
