;;; .* --- .*"  -*- lexical-binding: t; -*-
;;; Commentary:
;;; Code:


;; TODO: clean-up
(use-package tab-bar
  :general
  (k8x1d/leader-key
    "t" '(:keymap tab-prefix-map
		  :which-key "Tabs")
   )
  :hook
  (after-init . tab-bar-mode)
  :config
  (setq tab-bar-new-tab-choice "*scratch*")
  (setq tab-bar-show nil)
  (setq tab-bar-close-button-show nil)
  (setq tab-bar-new-button-show nil)
  (setq tab-bar-separator " ")

  ;; See https://lambdaland.org/posts/2022-07-20_adding_a_clock_to_emacs/
  (add-to-list 'tab-bar-format 'tab-bar-format-align-right 'append)
  (add-to-list 'tab-bar-format 'tab-bar-format-global 'append) ;; add to wm status bar

  (setq tab-bar-select-tab-modifiers '(meta))

  ;; Name adjustment
  (setq tab-bar-auto-width t)
  (setq tab-bar-auto-width-max '((100) 50))
					;
 ;; Set tab name to project vc name if exist
  (defun k8x1d/tab-bar-tab-name-current ()
    "Generate tab name from the projec-vc name or the buffer of the selected window."
    (cond (project-vc-name project-vc-name)
	  (t (buffer-name (window-buffer (or (minibuffer-selected-window)
					(and (window-minibuffer-p)
					     (get-mru-window)))))))
    )
  (setq tab-bar-tab-name-function #'k8x1d/tab-bar-tab-name-current)
  )

;; Persistent tab
;; (use-package tab-bookmark)

;; Integration with project.el
(use-package project-tab-groups
  :disabled
  :hook
  (after-init . project-tab-groups-mode)
  )

;; Show tab in echo area
;; Removed, unify better ui with firefox and other gui
(use-package tab-bar-echo-area
  :disabled
  :hook
  (tab-bar-mode . tab-bar-echo-area-mode)
  :config
  (setq tab-bar-show nil)
  (setq tab-bar-echo-area-display-tab-names-format-string "%s")
  (setq tab-bar-echo-area-format tab-bar-format)
  ;; (setq tab-bar-echo-area-display-tab-names-format-string "[%s]")
  (setq tab-bar-echo-area-display-tab-names-format-string "%s")
  (defun k8x1d/tab-bar-echo-area-format-tab-name-for-joining (name type _tab index count)
    "Format NAME according to TYPE, INDEX and COUNT."
    (format (cond ((eq type 'current-group) " %s ")
		  ((eq index (1- count)) " %s ")
		  (t " %s "))
	    name))
  (setq tab-bar-echo-area-format-tab-name-functions '(k8x1d/tab-bar-echo-area-format-tab-name-for-joining))

  ;; (setq tab-bar-tab-name-function #'tab-bar-tab-name-current-with-count)
  ;; (add-to-list 'tab-bar-echo-area-format 'tab-bar-format-align-right 'append)
  ;; (add-to-list 'tab-bar-echo-area-format 'tab-bar-format-global 'append)
  )


(provide 'tab-module)
;;; tab-module.el ends here
