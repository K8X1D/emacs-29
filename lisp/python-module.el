;;; .* --- .*"  -*- lexical-binding: t; -*-
;;; Commentary:
;;; Code:

;; Syntax highlight
(use-package python
  :bind
  ("C-c r p" . run-python)
  :config
  (setq python-interpreter "python3")
  )

;; ;; Treesitter support
(use-package treesit
  :config
  (add-to-list 'treesit-language-source-alist
	'(python "https://github.com/tree-sitter/tree-sitter-python"))
  (add-to-list 'major-mode-remap-alist
	'((python-mode . python-ts-mode)))
  (require 'org)
  (add-to-list 'org-src-lang-modes '("python" . python-ts))
)

;; REPL
(use-package python-vterm
  :general
  (k8x1d/leader-key
   "orp"  '(python-vterm-repl :which-key "Python")
   )
  :hook
  (python-mode . python-vterm-mode)
  (python-ts-mode . python-vterm-mode)
  :config
  (setq python-vterm-repl-program "ipython --no-banner")
  ;; (setq python-vterm-repl-program "ipython")
  )

;; Diagnostic
(use-package flymake
  :hook
  (python-mode . flymake-mode)
  (python-ts-mode . flymake-mode)
  )

;; LSP
(use-package eglot
  :hook
  (python-mode . eglot-ensure) ;; lsp
  (python-ts-mode . eglot-ensure) ;; lsp
  )

;; Snippets
(use-package yasnippet
  :hook
  (python-mode . yas-minor-mode)
  (python-ts-mode . yas-minor-mode)
  )

;; Notebook support
(use-package org
  :config
  (setq org-babel-python-command "python3")
  (add-to-list 'org-babel-load-languages '(python . t))
  (org-babel-do-load-languages 'org-babel-load-languages org-babel-load-languages)
  )

(use-package ob-python-vterm)
(use-package org
  :config
  (require 'ob-python-vterm)
  (add-to-list 'org-babel-load-languages '(python-vterm . t))
  (org-babel-do-load-languages 'org-babel-load-languages org-babel-load-languages)
  (defalias 'org-babel-execute:python 'org-babel-execute:python-vterm)
  (defalias 'org-babel-variable-assignments:python 'org-babel-variable-assignments:python-vterm)
  )

(provide 'python-module)
;;; python-module.el ends here
